using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;
using Unity.VisualScripting;

public class NextLevel : MonoBehaviour
{
    [Header("Variables of Next Level")]
    [SerializeField] float _waitTime = 5.0f;
    //[SerializeField] string _levelName;
    Vector3 boxScaleOG;
    [SerializeField] Vector3 boxScaleNew;
    BoxCollider _boxCollider;

    [SerializeField] public bool _ObjectiveComplete = true;

    [Header(" For Landing Text ")]
    [SerializeField] private GameObject _timerTextGM;
    private TextMeshPro _timerText;

    [Header("Cinematic Camera")]
    [Tooltip("Set to True if you are going to use the Dialogue for the Camera ")]
    [SerializeField] private bool _isdialog = false; 
    [SerializeField] private GameObject _cinematicCamera;

    PlayerController _player;

    [Header("Collectibles")]
    private int _collectibles;

    private bool _isLanded = false;

    [Header(" For Dialogue ")]
    [SerializeField] private bool _isEndDialogue = false;
    [SerializeField] private ConversationTrigger _EndDialogueTrigger;

    [SerializeField] private GameObject _objectiveMarker;
    [SerializeField] private ParticleSystem _psFireworks;

    [SerializeField] private LevelList _lvlList;
    private int _nextLevelINTforUnlock;
    private string _nxtLVL;
    private string _currentLVL;

    private float _timeTillRestart;

    [SerializeField] AudioSource _audioSource;



    [Header("END SCREEN INFO")]
    [SerializeField] private GameObject _endScreenWindow;
    [SerializeField] private TextMeshProUGUI _endTimeTMP;
    [SerializeField] private TextMeshProUGUI _endCollectibleTMP;
    [SerializeField] private TextMeshProUGUI _endFuelTMP;
    [SerializeField] private GameObject _endFirstSelected;
    // UI STARS WILL GO HERE 
    [SerializeField] private GameObject[] _stars = new GameObject[3];
    [SerializeField] private GameObject[] _starRings;
    [SerializeField] private GameObject _starsLightning;
    [SerializeField] TextMeshProUGUI[] _starTimingText = new TextMeshProUGUI[3];
    [SerializeField] private GameObject _extrastarPanel;
    // Delay Btw the star animations 
    [SerializeField] private float _delayBetweenAnimations = 0.1f; 

    [SerializeField] private LevelCompleteData _lvlData; // Scriptable Object for Level Information 

    [SerializeField] private Leaderboard _leaderboard;

    [SerializeField] private GameObject[] _Robots;


    //Steam Leaderboard 
    public UnityEvent e_levelFinish;
    
    

    // END SCREEN INFORMATION 
    private float p_fuel, p_time;
    private int p_collectibles;

    public static bool LevelComplete;

    private void Awake()
    {
        SetCam(); // Sets the Cinematic Camera 

        foreach(var robot in _Robots)
        {
            robot.transform.localScale = Vector3.zero;
        }
        _boxCollider = GetComponent<BoxCollider>();
        boxScaleOG = _boxCollider.size;
    }

    private void Start()
    {
        LevelComplete = false;
        _timeTillRestart = _waitTime;
        UpdateTimerText(_timeTillRestart);
        _timerText = _timerTextGM.GetComponent<TextMeshPro>();
        _timerTextGM.SetActive(false);
        _objectiveMarker.SetActive(false);
        _endScreenWindow.gameObject.SetActive(false);

        foreach (GameObject obj in _stars) obj.SetActive(false); //SET ALL THE STARS TO OFF 
        _starsLightning.SetActive(false);
        SetLVL(); // Sets the level List and the current and Next Level

        GameManager.levelComplete = false;

        
        
    }

    

    private void SetLVL()
    {
        int totalLVL = _lvlList.LevelName.Length;

        Scene currentScene = SceneManager.GetActiveScene();
        _currentLVL = currentScene.name;
        _nxtLVL = "L 0"; //By default the level will be set to the FIRST level

        // This Loop will compare the Current Level and the Level in the list and set the next level 
        for (int i = 1; i < totalLVL; i++)
        {
            if (_currentLVL == _lvlList.LevelName[i - 1])
            {
                _nxtLVL = _lvlList.LevelName[i];
                //Debug.Log("NEXT LEVEL WILL BE " + _nxtLVL);
                PlayerPrefs.SetInt("LastLevelIndex", i - 1);
                _nextLevelINTforUnlock = i; 
            }
        }

    }

    private void SetCam()
    {
        if (GameManager._Instance != null)
        {
            if(_cinematicCamera !=null)
                _cinematicCamera.SetActive(false);

            bool isNewLevel = GameManager._Instance._newLevelStarted;
            //Debug.Log("NEW LEVEL :" + isNewLevel);

            if (isNewLevel)
            {
                if (!_isdialog) // IF there is no existing Dialogue 
                {
                    if (_cinematicCamera != null)
                        _cinematicCamera.SetActive(true);

                }
            }
        }
        
    }

   

    private void Update()
    {
        if (_isLanded && _ObjectiveComplete)
        {
            if (_timeTillRestart <= 0 && _ObjectiveComplete &&_player.returnAliveStatus())
            {
                // THIS IS WHERE THE LEVEL IS FINISHED AND WE CAN PROCEED AHEAD IN THE GAME 


                _player.LVLFinish();
                float tempScore = _player.ReturnTime();
                tempScore = tempScore * 100;
                
                GameManager.SetScore( (int)tempScore); 

                e_levelFinish.Invoke();

                p_time = _player.ReturnTime() ;
                p_fuel = _player.ReturnFuelUsed();
                
                if (_EndDialogueTrigger != null)
                {
                    //_EndDialogueTrigger.StartConversation();
                    _isLanded = false;
                    Fireworks();
                    RobotsDancing();
                    //ShowScore();
                }
                else
                {
                   // NextLevelGO();
                    _isLanded = false;
                    Fireworks();
                    RobotsDancing();
                    ShowScore();
                    //Invoke("NextLevelGO", 5);

                }

            }
            else
            {
                if(_player.returnAliveStatus())
                {
                    _timeTillRestart -= Time.deltaTime;
                    UpdateTimerText(_timeTillRestart);
                }
                
            }
            
            

        }


    }

    private void Fireworks()
    {
        if (_psFireworks != null)
            _psFireworks.Play();

    }
    private void RobotsDancing()
    {
        //AudioManager.Instance.PlaySFX("Cheering");
        _audioSource.Play();


        Vector3 targetScale = new Vector3(0.5f,0.5f,0.5f);
        foreach(GameObject robot in _Robots)
        {
            
            robot.transform.DOScale(targetScale, 0.3f).SetEase(Ease.OutBounce);
            
        }
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" )
        {
            //Debug.Log(other);

            _boxCollider.size = boxScaleNew; 

            _isLanded = true;
            
            _player = other.gameObject.GetComponent<PlayerController>(); 

            if (!_ObjectiveComplete)
            {
                _objectiveMarker.SetActive(true);
                
            }
            else
            {

                _timerTextGM.SetActive(true);
            }

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            _isLanded = false;
            _timeTillRestart = _waitTime;
            UpdateTimerText(_timeTillRestart);
            _timerTextGM.SetActive(false);
            _objectiveMarker.SetActive(false);
            _boxCollider.size = boxScaleOG;
        }
            
        
    }

    public void NextLevelGO()
    {
        Debug.Log("CONGRATULATIONS ON COMPLETING THE LEVEL");
        Debug.Log("NEXT LEVELL" + _nxtLVL);
        GameManager.Nextlevel(_nxtLVL);

        if (PlayerPrefs.GetInt("LevelUnlocked") < _nextLevelINTforUnlock)
            PlayerPrefs.SetInt("LevelUnlocked", _nextLevelINTforUnlock);

    }

    public void ObjectiveComplete()
    {
        _ObjectiveComplete = true;
    }

  


    private void UpdateTimerText(float f)
    {
        
        if (_timerText != null)
        {
            
            _timerText.text = Mathf.Round(_timeTillRestart).ToString();
        }
        else
        {
        }
            
    }


    public void CollectibleScore(int total)
    {
        p_collectibles++; 
    }


    // NEED to get Collectbiel
    public void ShowScore()
    {
        float localScore = p_time * 100 ;
        PlayerPrefs.SetFloat(_currentLVL, localScore); 


        // Code for Time 
        GameManager.levelComplete = true;

        int _min = Mathf.FloorToInt(p_time / 60.0f);
        int _sec = Mathf.FloorToInt(p_time % 60.0f);
        float _millisec = (p_time * 100) % 100;  // Calculate milliseconds
        string _stopwatchString = string.Format("{0:0}:{1:00}.{2:0}", _min, _sec, _millisec);

        //Debug.Log("p_Time  :" + p_time);


        GameManager.LevelEnd();

        _lvlData.StarsDisplay(p_time);
        DisplayStars(p_time);

         _endScreenWindow.gameObject.SetActive(true);
        _endTimeTMP.text = _stopwatchString;
        //_endCollectibleTMP.text = p_collectibles.ToString();
        _endFuelTMP.text = MathF.Round(p_fuel).ToString() + " LTR";
        GameManager.LevelEnd();
        EventSystem.current.SetSelectedGameObject(_endFirstSelected);

        if (PlayerPrefs.GetInt("LevelUnlocked") < _nextLevelINTforUnlock)
            PlayerPrefs.SetInt("LevelUnlocked", _nextLevelINTforUnlock);



    }

    private void DisplayStars(float time)
    {
        // INPUT THE TIME AND GET THE NUMBER OF STARS FIRST 
        int starsCount = _lvlData.StarsDisplay(p_time); // This will calculate the number of stars; 
        Vector3 targetScale = new Vector3(1, 1, 1); // Set this to your original scale
        float duration = 0.3f;
        
        DG.Tweening.Sequence sequence = DOTween.Sequence();
        _starsLightning.transform.DOScale(Vector3.zero, 0f);

        Debug.Log("Stars :" + starsCount);

        if (true)
        {
            int i;
            
                for (i = 0; i < starsCount; i++)
                {
                    if (i<3)
                    {
                    _stars[i].gameObject.transform.localScale = Vector3.zero;
                    _stars[i].SetActive(true);
                    sequence.AppendCallback(() => StarSound());
                    sequence.Append(_stars[i].gameObject.transform.DOScale(targetScale, duration));
                    sequence.AppendInterval(_delayBetweenAnimations);
                    }
                   
                }
        }
        // THIS LOOP WILL MAKE THE STARS SMALLER AND GIVE THE BIGGG STARR LOLWA
        if(starsCount==4)
        {
            sequence.Append(_starsLightning.transform.DOScale(Vector3.zero, 0f));
            float delay = 0f;
            

            foreach(GameObject star in _stars)
                sequence.Append(star.transform.DOScale(Vector3.zero, delay));
            foreach (GameObject starRings in _starRings)
                sequence.Append(starRings.transform.DOScale(Vector3.zero, delay));


            _starsLightning.SetActive(true);
            sequence.Append(_starsLightning.transform.DOScale(Vector3.one, 2f).SetDelay(delay));

            _extrastarPanel.SetActive(false);
            sequence.Append(_extrastarPanel.transform.DOScale(Vector3.zero, 2f).SetDelay(delay));
        }



        int _min = Mathf.FloorToInt(p_time / 60.0f);
        int _sec = Mathf.FloorToInt(p_time % 60.0f);
        float _millisec = (p_time * 100) % 100;  // Calculate milliseconds
        string _stopwatchString = string.Format("{0:0}:{1:00}.{2:0}", _min, _sec, _millisec);


        
        _starTimingText[2].text = TimeString(_lvlData.starTiming(4));
        _starTimingText[1].text = TimeString(_lvlData.starTiming(3));
        _starTimingText[0].text = TimeString(_lvlData.starTiming(2));
    }

    

    private string TimeString(float time)
    {
        int _min = Mathf.FloorToInt(time / 60.0f);
        int _sec = Mathf.FloorToInt(time % 60.0f);
        float _millisec = (time * 100) % 100;  // Calculate milliseconds
        string _stopwatchString = string.Format("{0:0}:{1:00}.{2:0}", _min, _sec, _millisec);
        return _stopwatchString;
    }




    private void StarSound()
    {
        AudioManager.Instance.PlaySFX("Stars");
    }

 

    public void RestartLevel()
    {
        GameManager.Restart();
    }

}
