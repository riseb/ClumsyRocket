using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SkinManager : MonoBehaviour
{
    // Start is called before the first frame update
    public int _SkinIndex = 0;

    [SerializeField] GameObject[] _skins;

    

    private void Awake()
    {

        if (PlayerPrefs.HasKey("SkinIndex"))
        {
            _SkinIndex = PlayerPrefs.GetInt("SkinIndex");
            if (_SkinIndex > _skins.Length - 1) _SkinIndex = 0;
        }
        else
            _SkinIndex = 0;

        foreach (var sk in _skins)
        {
            sk.SetActive(false);
        }

        

        _skins[_SkinIndex].SetActive(true);
    }
}
