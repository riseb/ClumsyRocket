using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float _Delay;
    [SerializeField] private GameObject[] _camera;
    private int _cameraIndex;



    private void Awake()
    {
        if(_camera !=null)
        {
            if (GameManager._Instance._newLevelStarted)
            {
                //Debug.Log("STARTING THE CAMERA SEQUENCE");

                foreach (var camera in _camera) camera.SetActive(true);
            }
            else
            {
                foreach (var camera in _camera) camera.SetActive(false);
            }
        }
       
        if(_camera !=null)
        {
            _cameraIndex = _camera.Length;
            _cameraIndex = 0;
        }
        
        GameManager.introduceCameraManager(this);
    }

   
    

    public void RestartCamera()
    {
        Debug.Log("RESTART CAMERA  FROM CAMERA MANAGERE");
        foreach (var camera in _camera)
        {
            camera.SetActive(true);
            camera.GetComponent<DisableOnStart>().enabled = true;
        }

    }

    public void DeactivateCam()
    {
        if (_camera[_cameraIndex] != null )
            _camera[_cameraIndex].SetActive(false);

        if (_cameraIndex + 1 < _camera.Length)
            _cameraIndex++;
        else
            CancelInvoke("DeactivateCam");
    }

}
