using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotation : MonoBehaviour
{
    // Start is called before the first frame update
    public float _rotationSpeed = 50f;

    // Update is called once per frame
    void Update()
    {
        // Rotate the object around its Y axis at a constant speed
        transform.Rotate(0, 0, _rotationSpeed*Time.deltaTime);
    }
}
