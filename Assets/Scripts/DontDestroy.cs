using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    private static DontDestroy instance;

    // Start is called before the first frame update
    private void Awake()
    {
        DontDestroyOnLoad(this);
        if (instance == null)
        {
            // Set the instance to this object
            instance = this;
            // Make this object not destroyable on load
            
        }
        else
        {
            // Destroy the new duplicate object to maintain a single instance
            Destroy(gameObject);
        }
    }
}
