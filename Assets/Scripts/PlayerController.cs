using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{

    [SerializeField] private InputReader _input;

    PlayerController _thisplayer;
    [Header("Thruster Location")]
    [SerializeField] private Transform _tLThruster;
    [SerializeField] private Transform _tRThruster;
    [SerializeField] private Transform _tMthruster;
    
    [Header("Particle System")]
    [SerializeField] private ParticleSystem _PS_LThruster;
    [SerializeField] private ParticleSystem _PS_RThruster;
    [SerializeField] private ParticleSystem _vfx_Explosion;

    [Header("Sound")]
    [SerializeField] private AudioSource _sfxThruster;
    [SerializeField] private AudioSource _sfxRThruster;

    [Header("Mesh")]
    [SerializeField] private GameObject _DeadBody;
    [SerializeField] private GameObject _ThisBody;
    [SerializeField] private GameObject _goalMarker;

    [Header("Canvas & Tutorial")]
    [SerializeField] private GameObject _restartUI;

    private Rigidbody _rb;
    private Transform _thruster;
    private Vector3 _force;
    private int _combinedValue = 0; //This is for the switch case of the Thrusters  

    [Header("Rocketship Parameters")]
    //PLAYER MODIFYABLE 
    [SerializeField] private bool _isFuelLevel;
    [SerializeField] private float _thrusterPower = 200f;
    [SerializeField] private float _fuel = 100.0f;
    [SerializeField] private float _fuelMinus = 1.0f ;
    private float _fuelused = 0;

    [Header("Idle Timer Instructions")]
    [SerializeField] private float _timeTillTutorialText;
    private float _IdleTimer;
    [SerializeField] private GameObject _tutorialText; // 

    [Header("HUD")]
    
    [SerializeField] TextMeshProUGUI _FuelText;

    [SerializeField] private TextMeshProUGUI _levelTimerText;
    private float _levelTime;
    int _min, _sec;
    float _millisec;
    string _stopwatchString;
    bool _startLVLTimer = false;

    private bool _Lthrust, _RThrust, _isAlive, _isLevelComplete;

    private bool _cameraRestartBool; 

    [SerializeField] private RocketAttatch _attatchmentScript;

    public UnityEvent LavaDeathEvent,onDeathEvent ;

    private void Awake()
    {
        if (_DeadBody != null)
        {
            _DeadBody.SetActive(false);

        }
        if (_restartUI != null)
            _restartUI.SetActive(false);

        _thisplayer = this.GetComponent<PlayerController>();

        if (!_isFuelLevel)
        {
            _FuelText.gameObject.SetActive(false);
        }
        _cameraRestartBool = false;
    }

    private void Start()
    {
        //Initialising the Thrusterz 
        _Lthrust = false;
        _RThrust = false;
        _combinedValue = 0;
        _isAlive = true;
        _startLVLTimer = false;
        _isLevelComplete = false;
        SubscribeEvents(); // Subscribing Methods to the Events 
        _rb = GetComponent<Rigidbody>();
        AddAttatchemnt();

        _tutorialText.SetActive(false);
        
    }

    public bool returnAliveStatus()
    {
        return _isAlive;
    }

    public void SubscribeEvents()
    {

        UnSubscribeEvents();

        if(!GameManager._isControlsInverted)
        {
            _input.LeftThrusterEvent += LeftThrusterActivate;
            _input.RightThrusterEvent += RightThrusterActivate;
            _input.LeftThrusterCancelledEvent += LeftThrusterCancel;
            _input.RightThrusterCancelledEvent += RightThrusterCancel;
            _input.RestartEvent += RestartGame;
            _input.RestartCameraEvent += RestartCamera; 
            _input.InteractAction += UseAttatchment;
        }
        else
        {
            _input.LeftThrusterEvent +=  RightThrusterActivate;
            _input.RightThrusterEvent += LeftThrusterActivate;
            _input.LeftThrusterCancelledEvent +=  RightThrusterCancel;
            _input.RightThrusterCancelledEvent += LeftThrusterCancel;
            _input.RestartEvent += RestartGame;
            _input.RestartCameraEvent += RestartCamera;
            _input.InteractAction += UseAttatchment;
        }

    }

    private void UnSubscribeEvents()
    {
        // Subscribing Methods to the Events 
        _input.LeftThrusterEvent -= LeftThrusterActivate;
        _input.RightThrusterEvent -= RightThrusterActivate;
        _input.LeftThrusterCancelledEvent -= LeftThrusterCancel;
        _input.RightThrusterCancelledEvent -= RightThrusterCancel;
        _input.RestartEvent -= RestartGame;
        _input.RestartCameraEvent -= RestartCamera;
        _input.InteractAction -= UseAttatchment;

        _input.LeftThrusterEvent -= RightThrusterActivate;
        _input.RightThrusterEvent -= LeftThrusterActivate;
        _input.LeftThrusterCancelledEvent -= RightThrusterCancel;
        _input.RightThrusterCancelledEvent -= LeftThrusterCancel;
        _input.RestartEvent -= RestartGame;
        _input.InteractAction -= UseAttatchment;
    }

    private void OnDestroy()
    {
        UnSubscribeEvents();
        //onDeathEvent = null;
    }
    private void OnDisable()
    {
        //onDeathEvent = null;
    }


    public void AddAttatchemnt()
    {
        if(_attatchmentScript !=null)
        {
            _attatchmentScript.Attach(this.transform.position, this.gameObject);
            
        }
    }

    private void UseAttatchment()
    {
        
        if (_attatchmentScript != null)
        {
            _attatchmentScript.Action();
        }
        if (_attatchmentScript == null)
        {
            Debug.LogWarning("No Attatchment Script Found ");
        }
            
    }

    private void FixedUpdate()
    {
        ThrusterThrust();

        if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("Nothing to Debug right now ");
        }
    }

    private void Update()
    {
        UpdateLevelTimer(); // This is for the level timer 

        if(!_isLevelComplete) // Checks if level is completed 
        {
            //This is for the TutorialText
            if (_tutorialText != null)
            {
                if (_IdleTimer > _timeTillTutorialText)
                    _tutorialText.SetActive(true);
                else
                    _tutorialText.SetActive(false);
            }
        }
        
    }


    private void UpdateLevelTimer()
    {
        if (_startLVLTimer)
            _levelTime += Time.deltaTime;

        _IdleTimer += Time.deltaTime; 

        // Format the elapsed time as minutes:seconds:milliseconds (e.g., "1:30.0")
        _min = Mathf.FloorToInt(_levelTime / 60.0f);
        _sec = Mathf.FloorToInt(_levelTime % 60.0f);
        _millisec = (_levelTime * 100) % 100;  // Calculate milliseconds
        _stopwatchString = string.Format("{0:0}:{1:00}.{2:0}", _min, _sec, _millisec);

        if (_levelTimerText != null && ! _isLevelComplete)
        {
            _levelTimerText.text = _stopwatchString;
        }

    }
    

    /// THIS CODE IS for the Thruster UPDATE 
    private void ThrusterThrust()
    {
        _combinedValue = 0; // Combined Value is to choose which Thruster to thrusst 
        
        if (_Lthrust)
            _combinedValue += 1;
        if (_RThrust)
            _combinedValue += 2;



        //Debug.Log("Combined Value : "+_combinedValue);


        switch (_combinedValue)
        {
            case 0: 
                //  NO THRUSTER WORKING  
                _force = Vector3.zero;
                break;

            case 1:
                // LEFT THRUSTER IS WORKING
                // Condition 1 is ture
                //Debug.Log("Left");
                _startLVLTimer = true;
                _thruster = _tLThruster;
                _force = _thrusterPower * _thruster.up;
                _rb.AddForceAtPosition(_force, _thruster.position, ForceMode.Force);
                break;

            case 2:
                // RIGHT THRUSTER IS WORKING 
                //Condiotion 2 is True 
                //Debug.Log("Right");
                _startLVLTimer = true;
                _thruster = _tRThruster;
                _force = _thrusterPower * _thruster.up;
                _rb.AddForceAtPosition(_force, _thruster.position, ForceMode.Force);
                break;

            case 3:
                // BOTH THRUSTERS ARE WORKING 
                // Both condition1 and condition2 are true
                //Debug.Log("Center");
                _startLVLTimer = true;
                _thruster = _tMthruster;
                _force = _thrusterPower * 2 * _thruster.up;
                _rb.AddForceAtPosition(_force, _thruster.position,ForceMode.Force);
                break;

            default:
                
                break;
        }

        FuelDeplete(_combinedValue);
    }

    
    private void RestartCamera()
    {
        //CODE FOR RESTART CAMERA 
        if(!_cameraRestartBool)
        {
            GameManager.restartCamera();
            _cameraRestartBool = true;
        }
        else
        {
            _cameraRestartBool = false;
        }
        

        
    }

    private void RightThrusterActivate()
    {
        _RThrust = true;
        //_combinedValue += 2;

        if (_PS_RThruster != null && _isAlive) 
        {
            _PS_RThruster.Play();
            if (_sfxRThruster != null && !_sfxRThruster.isPlaying)
                _sfxRThruster.Play();
            
            Cursor.visible = false;
            _IdleTimer = 0; 
        }
        RumbleManager.instance.RumbleStart();
    }

    private void LeftThrusterActivate()
    {
        _Lthrust = true;
        //_combinedValue += 1; 
        if (_PS_LThruster != null && _isAlive)
        {
            _PS_LThruster.Play();
            
            if(_sfxThruster != null && !_sfxThruster.isPlaying)
                _sfxThruster.Play();

            Cursor.visible = false;
            _IdleTimer = 0; 

        }
        RumbleManager.instance.RumbleStart();

    }

    private void LeftThrusterCancel()
    {
        _Lthrust = false;
        //_combinedValue -= 1;

        if (_PS_LThruster != null)
        {
            _PS_LThruster.Stop();

            if (_sfxThruster != null)
                _sfxThruster.Stop();
        }

        if (!_RThrust)
        { // Stop the rumble if the Lthruster is inactive
            RumbleManager.instance.RumbleStop();
        }
    }

    private void RightThrusterCancel()
    {
        _RThrust = false;
        //_combinedValue -= 2;

       

        if (_PS_RThruster != null)
        {
            _PS_RThruster.Stop();
            
            if (_sfxRThruster != null )
                _sfxRThruster.Stop();
        }

        if (!_Lthrust)
        { // Stop the rumble if the Lthruster is inactive
            RumbleManager.instance.RumbleStop();
        }
    }

    private void RestartGame()
    {
        if (!_cameraRestartBool)
        {
            GameManager.Restart();
            _cameraRestartBool = false;
        }

    }



    private void FuelDeplete(int i)
    {

        _fuelused += (_fuelMinus * i) * Time.deltaTime;
        if (_isFuelLevel) // REPLACE THIS WITH bool _FuelGameMode when you make that script to activate with game mode 
        {
            _fuel = _fuel - (_fuelMinus * i)*Time.deltaTime;
            //Debug.Log("Fuel is "+_fuel);
            FuelUI();
        }

        if( _fuel <=0 )
        {
            Death();
        }

    }

    public void FuelReplete()
    {
        if (_fuel <= 100)
        {

            _fuel += _fuelMinus * Time.deltaTime * 10;
            FuelUI();
        }
        
    }

    public void FuelUI()
    {
       if (_FuelText != null)
        {
            _FuelText.text = Mathf.Round(_fuel).ToString() + " %";
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.gameObject.tag);

        if (collision.gameObject.tag == "OutBound")
        {
            Death();
           
            if( LavaDeathEvent!=null )
            {
                LavaDeathEvent.Invoke();
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        // YOINK THE PLAYER DED 
        //Debug.Log(other);
        if (other.tag == "Ground")
        {
            Death();
            
        }
        
    }

    public void LVLFinish()
    {
        _startLVLTimer = false;

        _isLevelComplete = true;
    }

    public void Death()
    {
        LVLFinish();

        

        // OPENS UP THE DEATH SCREEN ( PRESS R TO RESTART ) 
        if (_restartUI != null)
        {
            if (GameManager._Instance._playerRestartHud)
                _restartUI.SetActive(true);
        }
        else
            Debug.LogError("no REspawn ui Screen ");

        RumbleManager.instance.RumblePulse();

        // Code for mostly Turning things OFF 

       
       

        if (_isAlive)
        {

            _sfxRThruster.Stop();
            _sfxThruster.Stop();
            _PS_LThruster.Stop();
            _PS_RThruster.Stop();
            
            if (_vfx_Explosion != null)
            {
                _vfx_Explosion.Play();
                AudioManager.Instance.PlaySFX("Death");

            }

            if (_ThisBody != null)
            {
                _ThisBody.SetActive(false);

            }

            if (_DeadBody != null)
            {
                _DeadBody.SetActive(true);
            }

            _thisplayer.enabled = false;

            if(_attatchmentScript != null )
            {
                Destroy(_attatchmentScript.gameObject);
            }


            if (_goalMarker != null)
                _goalMarker.SetActive(false);



            _isAlive = false;

            // DIES ONLY ONCE 
            if (onDeathEvent != null)
            {
                onDeathEvent.Invoke();
                onDeathEvent = null;
            }
        }
        
    }


    public float ReturnTime()
    {
        return _levelTime;
    }
    public float ReturnFuelUsed()
    {
        return _fuelused;
    }

    public void RumbleController()
    {

    }

}
