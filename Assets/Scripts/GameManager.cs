using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    bool _isPause, _isDialogue, _isSetting;

    public static bool _isControlsInverted = false;
    public static bool _CinematicSkip = false;

    public static int _device; // 0 - keyboard, 1- controller XBOX, 2- Playstation Controller  

    
    public static GameManager _Instance;
    [SerializeField] private InputReader _input;

    [SerializeField] GameObject _pauseMenu;
    

    [Header("First Selected Options")]
    [SerializeField] GameObject _PauseMenuFirst;

    [SerializeField] public string _playerName;

    [SerializeField] DeviceDetectScript _deviceDetectScript;

    public static bool levelComplete;

    public int _score= 0;
   
    private bool _isLevel; // THIS bool will let the game manager know if the player is in a level or not, Might not need this but lets see 

    
    public  bool _newLevelStarted; // This bool is to differentiate if a new Level is Loaded or  Not 

    [SerializeField] ConversationTrigger _currentConversation;


    private PlayerController _playerScript;

    private CameraManager _cameraManager;

    [SerializeField] public bool _playerRestartHud = true; // IF FALSE MAKE THE HUD NULL 

    private void Awake()
    {
        if (_Instance == null)
        {
            _Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        

        _Instance._isPause = false;
        _input.PauseEvent += PauseGame;
        //_input.RestartEvent += Restart;
        _input.NextOptionEvent += NextOption;
        _input.PrevOptionEvent += PrevOption;
        _input.SelectOptionEvent += SelectOption;
        _input.SkipDialogEvent += SkipDialog;

        //_input.BackButtonEvent += BackButton; // Back B
        //_input.onApplyButton += ApplyButton; // Apply Settings 

        _isLevel  =false;
        _isSetting = false;
        _newLevelStarted = true;

        _device = 0;

        PlayerPrefs.SetInt("Skin0", 1);
    }

    

    private void OnEnable()
    {
        _input.SetUI();
    }


    private void subscribeButtons()
    {

    }
    
    private void UnsubscribeButtons()
    {

    }



    private void Start()
    {
        if (_pauseMenu != null)
        {
            _pauseMenu.SetActive(false);
        }
    }


    public static void Restart()
    {
        
         _Instance._newLevelStarted = false;
        Resources.UnloadUnusedAssets();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        _Instance.UnPause();

    }

    

    public static void PauseGame()
    {
        Debug.Log("Pause Game");

        if (!_Instance._isPause)
            _Instance.Pause();
        else
            _Instance.UnPause();
    }

    public static void Nextlevel(string levelName)
    {
        _Instance._newLevelStarted = true;
        SceneManager.LoadScene(levelName);


        if (_Instance._deviceDetectScript != null)
        {
            //Debug.Log(" Detect Glyphs");

            _Instance._deviceDetectScript.detectGlyphScripts();
        }
    }

    public static void introduceCameraManager(CameraManager cameraManager)
    {
        _Instance._cameraManager = cameraManager;
        //Debug.Log(cameraManager.name);
    }
    
    public static void restartCamera()
    {
        //Debug.Log(" RESTART THE CAMERA ");
        if( _Instance._cameraManager != null )
        {
            _Instance._cameraManager.RestartCamera();
        }

    }

    private void Pause()
    {
        if(_device == 0)
         Cursor.visible = true;

        if(_pauseMenu == null)
            _pauseMenu = GameObject.FindGameObjectWithTag("PauseMenu");
        
        if (_pauseMenu != null)
        {
            _pauseMenu.SetActive(true);
            _isPause = true;
            Time.timeScale = 0f;
            _input.SetUI();

            if (_Instance._PauseMenuFirst != null)
                EventSystem.current.SetSelectedGameObject(_PauseMenuFirst);
        }
        
    }

    public void UnPause()
    {
        Cursor.visible = false;
        //ADD MORE CONDITIONS FOR UNPAUSING WHEN THE GAME IS PAUSED 
        if (_pauseMenu == null)
            _pauseMenu = GameObject.FindGameObjectWithTag("PauseMenu");

        if (_pauseMenu != null)
        {
            Time.timeScale = 1f;
            _input.SetGameplay();
            _pauseMenu.SetActive(false);
            _isPause = false;
        }
        
    }

    public static void LevelEnd()
    {
        _Instance._input.SetDialogue();
        RumbleManager.instance.RumbleStop();
        if(_device == 0)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;   
        }

        GameObject button = GameObject.FindGameObjectWithTag("Button");
        if (button != null)
            EventSystem.current.SetSelectedGameObject(button);

    }    

    public void SetNewLevelStartedFalse()
    {
        _Instance._newLevelStarted = false;
        _newLevelStarted = false; 
    }


    public static void LevelStart()
    {
        _Instance.UnPause();

        Cursor.visible = false;
    }

    public static void StartDialogue(ConversationTrigger convo)
    {
        return;

        _Instance._currentConversation = convo;

        _Instance._isDialogue = true;
        _Instance._input.SetDialogue();
    }

    public static void EndDialogue()
    {
        
        // NEED TO CHECK HERE IF THE LEVEL IS FINISHED OR NOT 

        if (levelComplete)
        {
            // TRUE 
            _Instance._isDialogue = false;
            _Instance._input.SetDialogue();
        }
        else
        {
            // FALSE
            _Instance._isDialogue = false;
            _Instance._input.SetGameplay();
        }
        

    }

    private void SelectOption()
    {
        if (_currentConversation != null)
        {
            _currentConversation.SelectOption();
        }
    }

    private void PrevOption()
    {
        if (_currentConversation != null)
        {
            _currentConversation.PreviousOption();
        }
    }

    private void NextOption()
    {
        if (_currentConversation != null)
        {
            _currentConversation.NextOption();
        }
    }

    private void SkipDialog()
    {
        if(_currentConversation != null)
        {
            //Also code for Skip Cinematic
            
            _currentConversation.SkipDialog();
            EndDialogue();


            GameObject gm = GameObject.FindGameObjectWithTag("cinematic");
            if (gm != null)
            {
                gm.SetActive(false);
            }
            

        }
    }

    public static void InvertControlsGM()
    {
        _isControlsInverted = !_isControlsInverted;

        if (_Instance._playerScript == null)
        {
            GameObject gm = GameObject.FindGameObjectWithTag("Player");
            _Instance._playerScript = gm.GetComponent<PlayerController>();
            Debug.Log(gm);


        }
        // Player subscribe 

        if (_Instance._playerScript != null)
        {
            _Instance._playerScript.SubscribeEvents();
        }
        
        
    }

    public static void SetScore(int s)
    {
        //Debug.Log("Gamemanager Score : " + s);

        _Instance._score = s;
    }

    public static int GetScore()
    {
        return _Instance._score;
    }

    public static int DeviceInteger()
    {
        return _device; 
    }


    public void BackButton()
    {
        // USED WHEN THE BACK BUTTON IS PRESSED 
        GameObject BackButtonOBJ = null;
        Button backButton = null;

        BackButtonOBJ = GameObject.FindGameObjectWithTag("BackButton");
        backButton = BackButtonOBJ.GetComponent<Button>();

        if (BackButtonOBJ != null)
            EventSystem.current.SetSelectedGameObject(BackButtonOBJ);

        Debug.Log("Back Button : "+backButton);

        if (backButton != null)
            backButton.onClick.Invoke();
        else
            Debug.LogWarning("BACK BUTTON NOT FOUND RISAB");

    }

    private void ApplyButton()
    {
        // Used to Apply Graphics and Shit 
        GameObject ApplyButtonOBJ = null;
        Button ApplyButton = null;

        ApplyButtonOBJ = GameObject.FindGameObjectWithTag("ApplyButton");
        ApplyButton = ApplyButtonOBJ.GetComponent<Button>();

        if (ApplyButtonOBJ != null)
            EventSystem.current.SetSelectedGameObject(ApplyButtonOBJ);

        Debug.Log("Back Button : " + ApplyButton);

        if (ApplyButton != null)
            ApplyButton.onClick.Invoke();
        else
            Debug.LogWarning("APPLY BUTTON DOESNT EXIST BITCH");
    }
}
