using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class SimpleSwitch : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject _setObject;
    [SerializeField] GameObject _placeHolder;

    [SerializeField] bool _SetState;
    [SerializeField] bool _deactivateOnceActive = false;
    public UnityEvent _SimpleSwitchEvent;

    private void Awake()
    {
        _placeHolder.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" )
        {

            Debug.Log("Activate");


            _setObject.SetActive(_SetState);
           
            if(_SimpleSwitchEvent != null)
            {
                _SimpleSwitchEvent.Invoke();
            }



            if(_deactivateOnceActive)
            {
                gameObject.SetActive(false);
            }
                
        }

        
    }

   
}
