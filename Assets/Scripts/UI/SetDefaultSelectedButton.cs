using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SetDefaultSelectedButton : MonoBehaviour
{
    public GameObject defaultSelectedButton;

    void Start()
    {
        if (defaultSelectedButton != null)
        {
            EventSystem.current.SetSelectedGameObject(defaultSelectedButton);
        }
    }
}
