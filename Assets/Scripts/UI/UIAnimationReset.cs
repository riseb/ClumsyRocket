using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAnimationReset : MonoBehaviour
{
    public Animator animator;

    private void OnEnable()
    {
        ResetUIAnimation();
    }


    public void ResetUIAnimation()
    {
        animator.Play("Normal", -1, 0f);
    }
}
