using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Unity.VisualScripting;

[RequireComponent(typeof(Image))]
public class TabButton : MonoBehaviour, ISelectHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public GameObject page;
    public TabGroup tabGroup;

    public Image background;

    private void Start()
    {
        background = GetComponent<Image>();
        tabGroup.Subscribe(this);

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        tabGroup.OnTabSelected(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        tabGroup.OnTabEnter(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tabGroup.OnTabExit(this);
    }

    public void OnSelect(BaseEventData eventData)
    {
        tabGroup.OnTabSelected(this); // Switch to this tab's page
    }

    // Called when this button is deselected
    /*public void OnDeselect(BaseEventData eventData)
    {
        tabGroup.OnTabDeselected(this); // Optionally deactivate page or perform other actions
    }*/

    public void ActivateTabPage()
    {
        if (page != null)
        {
            page.SetActive(true); // Make the page active
        }
    }

    // Deactivates the associated page for this tab
    public void DeactivateTabPage()
    {
        if (page != null)
        {
            page.SetActive(false); // Hide the page when the tab is deselected
        }
    }

    public void Select()
    {

    }

    public void Deselect()
    {

    }
}
