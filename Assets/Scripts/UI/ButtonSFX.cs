using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonSFX : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, ISelectHandler, ISubmitHandler
{
    
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private StartMenu sfxParent;

    private void Start()
    {
        sfxParent = FindObjectOfType<StartMenu>();
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        PlayHoverSFX();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        PlayClickSFX();
    }

    public void OnSelect(BaseEventData eventData)
    {
        PlayHoverSFX();
    }
    public void OnSubmit(BaseEventData eventData)
    {
        PlayClickSFX();
    }

    private void PlayHoverSFX()
    {
        if (sfxParent.hoverSFX != null)
        {
            audioSource.PlayOneShot(sfxParent.hoverSFX);
        }
    }

    // Play the click sound effect
    private void PlayClickSFX()
    {
        if (sfxParent.clickSFX != null)
        {
            audioSource.PlayOneShot(sfxParent.clickSFX);
        }
    }
}
