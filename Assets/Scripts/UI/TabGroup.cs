using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class TabGroup : MonoBehaviour
{
    [SerializeField] private Button displayTabButton;
    [SerializeField] private Button soundTabButton;
    [SerializeField] private Button controlsTabButton;

    [SerializeField] private GameObject displayPanel;
    [SerializeField] private GameObject soundPanel;
    [SerializeField] private GameObject controlsPanel;

    private List<TabButton> tabButtons;

    public Sprite tabIdle;
    public Sprite tabHover;
    public Sprite tabActive;
    [SerializeField] private TabButton selectedTab;
    public List<GameObject> objectsToSwap;

    public TabButton defaultTab;

    public PlayerInput playerInputActions;



    private void Start()
    {
        if (defaultTab != null)
        {
            OnTabSelected(defaultTab);
        }
        else if (tabButtons.Count > 0)
        {
            OnTabSelected(tabButtons[0]);
        }

        //playerInputActions.UI.Back.performed += ctx => OnBackPressed();
    }

    private void OnBackPressed()
    {
        // Handle the Back button press to go back to the tab view
        ShowTabView();
    }

    public void Subscribe(TabButton button)
    {
        if(tabButtons == null)
        {
            tabButtons = new List<TabButton>();
        }
        tabButtons.Add(button);
    }

    public void OnTabEnter(TabButton button)
    {
        ResetTabs();
        if (selectedTab != null || button != selectedTab)
        {
            button.background.sprite = tabHover;
        }
    }

    public void OnTabExit(TabButton button)
    {
        ResetTabs();
    }

    public void OnTabSelected(TabButton button)
    {
        if (selectedTab != null)
        {
            selectedTab.Deselect();
        }

        selectedTab = button;

        selectedTab.Select();

        ResetTabs();
        button.background.sprite = tabActive;

        if (selectedTab.CompareTag("Display"))
        {
            ShowDisplayTab();
        }
        else if (selectedTab.CompareTag("Sound"))
        {
            ShowSoundTab();
        }
        else if (selectedTab.CompareTag("Controls"))
        {
            ShowControlsTab();
        }

        int index = button.transform.GetSiblingIndex();
        for (int i = 0; i < objectsToSwap.Count; i++)
        {
            if(i== index)
            {
                objectsToSwap[i].SetActive(true);
            }
            else
            {
                objectsToSwap[i].SetActive(false);
            }
        }

        
    }

    public void ResetTabs()
    {
        foreach (TabButton button in tabButtons)
        {
            if(selectedTab != null && button == selectedTab) { continue; }
            button.background.sprite = tabIdle;
        }
    }

    public void ShowDisplayTab()
    {
        // Hide all panels

        Debug.Log("InsideDisplayTab");
        displayPanel.SetActive(true);
        soundPanel.SetActive(false);
        controlsPanel.SetActive(false);

        /*displayTabButton.interactable = false;
        soundTabButton.interactable = false;
        controlsTabButton.interactable = false;*/

        //EnablePanelButtons(displayPanel);

    }

    public void ShowSoundTab()
    {
        Debug.Log("InsideSoundTab");
        // Hide all panels
        displayPanel.SetActive(false);
        soundPanel.SetActive(true);
        controlsPanel.SetActive(false);

        // Disable tab buttons
        /*displayTabButton.interactable = false;
        soundTabButton.interactable = false;
        controlsTabButton.interactable = false;*/

        //EnablePanelButtons(soundPanel);
    }

    public void ShowControlsTab()
    {
        // Hide all panels
        displayPanel.SetActive(false);
        soundPanel.SetActive(false);
        controlsPanel.SetActive(true);

        // Disable tab buttons
        /*displayTabButton.interactable = false;
        soundTabButton.interactable = false;
        controlsTabButton.interactable = false;*/

        //EnablePanelButtons(controlsPanel);

    }

    void ShowTabView()
    {
        // Hide all panels (go back to tab view)
        displayPanel.SetActive(false);
        soundPanel.SetActive(false);
        controlsPanel.SetActive(false);

        // Enable tab buttons
        displayTabButton.interactable = true;
        soundTabButton.interactable = true;
        controlsTabButton.interactable = true;


    }

    void EnablePanelButtons(GameObject panel)
    {
        // Example: enable all buttons inside the given panel
        Button[] panelButtons = panel.GetComponentsInChildren<Button>();
        foreach (Button button in panelButtons)
        {
            button.interactable = true;
        }
    }


}
