using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DialogueEditor; 

public class Dialog : MonoBehaviour
{
    [SerializeField] private InputReader _input;

    private void OnEnable()
    {
        ConversationManager.OnConversationStarted += ConversationStart;
        ConversationManager.OnConversationEnded += ConversationEnd;
    }

    private void OnDisable()
    {
        ConversationManager.OnConversationStarted -= ConversationStart;
        ConversationManager.OnConversationEnded -= ConversationEnd;
       }

    private void ConversationStart()
    {
        Debug.Log("A conversation has began.");
    }

    private void ConversationEnd()
    {
        Debug.Log("A conversation has ended.");
    }


}
