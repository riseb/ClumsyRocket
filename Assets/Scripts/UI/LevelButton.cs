#if !(UNITY_STANDALONE_WIN || UNITY_STANDALONE_LINUX || UNITY_STANDALONE_OSX || STEAMWORKS_WIN || STEAMWORKS_LIN_OSX)
#define DISABLESTEAMWORKS
#endif


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

#if !DISABLESTEAMWORKS
using Steamworks;
#endif

public class LevelButton : MonoBehaviour
{
    public string loadLevel;
    public string levelLeaderboard;


    [SerializeField] string s_leaderboardName;
#if !DISABLESTEAMWORKS
    private SteamLeaderboard_t s_currentLeaderboard;
    private bool s_initialized = false;
    private CallResult<LeaderboardFindResult_t> m_findResult = new CallResult<LeaderboardFindResult_t>();
    private CallResult<LeaderboardScoreUploaded_t> m_uploadResult = new CallResult<LeaderboardScoreUploaded_t>();
    private CallResult<LeaderboardScoresDownloaded_t> m_downloadResult = new CallResult<LeaderboardScoresDownloaded_t>();
#endif
    [Header("LeaderBoard UI Elements")]
    [SerializeField] List<TextMeshProUGUI> _leaderboardRanks = new List<TextMeshProUGUI>();
    [SerializeField] List<TextMeshProUGUI> _leaderboardName = new List<TextMeshProUGUI>();
    [SerializeField] List<TextMeshProUGUI> _leaderboardScore = new List<TextMeshProUGUI>();
    [SerializeField] TextMeshProUGUI _localScore;
    [SerializeField] public TextMeshProUGUI _levelName;
    [SerializeField] public GameObject _lightnightStarHidder;

    [Header("Image")]
    [SerializeField] public Image _lvlImage;


    [Header("LEVEL MENU LEVELS")]
    [SerializeField] private TextMeshProUGUI star1Time;
    [SerializeField] private TextMeshProUGUI star2Time;
    [SerializeField] private TextMeshProUGUI star3Time;



    //Just Variables 
    private float _lightningStar;

    private void Start()
    {

#if !DISABLESTEAMWORKS
        //STEAM MANAGER
        if (SteamManager.Initialized)
        {
            string name = SteamFriends.GetPersonaName();
            //Debug.Log(name);
            // s_leaderboardName = levelLeaderboard;
            //GetLeaderboard();

        }
#endif
    }

    public void LoadLeaderboard(string leaderboard)
    {
#if !DISABLESTEAMWORKS
        s_leaderboardName = leaderboard; 
        GetLeaderboard() ;
#endif
    }



    public void StarsInit(float star1, float star2, float star3)
    {
        //  STARS  //
        star1Time.text = star3.ToString();
        star2Time.text = star2.ToString();
        star3Time.text = star1.ToString();
        _lightningStar = star1;
    }
#if !DISABLESTEAMWORKS
    // LEADERBOARD CRAP 
    public void GetLeaderboard()
    {

        if (SteamManager.Initialized)
        {
            SteamAPICall_t hSteamAPICall = SteamUserStats.FindLeaderboard(s_leaderboardName);
            m_findResult.Set(hSteamAPICall, GettingLeaderboard);
        }

    }

    public void GettingLeaderboard(LeaderboardFindResult_t pCallback, bool failure)
    {
        //Debug.Log($"Steam Leaderboard Find: Did it fail? {failure}, Found: {pCallback.m_bLeaderboardFound}, leaderboardID: {pCallback.m_hSteamLeaderboard.m_SteamLeaderboard} ");
        s_currentLeaderboard = pCallback.m_hSteamLeaderboard;
        s_initialized = true;

        //Debug.Log("LeaderboardFound! Time to Get Score ");


        SteamAPICall_t hSteamAPICall = SteamUserStats.DownloadLeaderboardEntries(s_currentLeaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal, 1, 10);
        m_downloadResult.Set(hSteamAPICall, GetScore);
    }


    public void GetScore(LeaderboardScoresDownloaded_t pCallback, bool failure)
    {
        string scorestr;
        float displayscore;
        if (!failure)
        {
            //Debug.Log(pCallback.m_cEntryCount);

            for (int i = 0; i < pCallback.m_cEntryCount; i++)
            {
                LeaderboardEntry_t entry;
                SteamUserStats.GetDownloadedLeaderboardEntry(pCallback.m_hSteamLeaderboardEntries, i, out entry, null, 0);

                //Debug.Log($"User {SteamFriends.GetFriendPersonaName(entry.m_steamIDUser)} - {entry.m_nScore} - rank: {entry.m_nGlobalRank}");

                if (_leaderboardRanks[i] != null)
                    _leaderboardRanks[i].text = entry.m_nGlobalRank + ". ";

                if (_leaderboardName[i] != null)
                    _leaderboardName[i].text = SteamFriends.GetFriendPersonaName(entry.m_steamIDUser).ToString();

                //this code will need to have the conversion of the score
                if (_leaderboardScore[i] != null)
                {
                    displayscore = entry.m_nScore;
                    displayscore = displayscore / 100;
                    scorestr = displayscore.ToString() + " s";
                    _leaderboardScore[i].text = scorestr;
                }


            }

        }
        else
            Debug.Log("Failed to Retreive Leaderbaord Scores ");
    }
#endif
    public void LocalHighScore(string lvlName)
    {
        
        float localScore = PlayerPrefs.GetFloat(lvlName);

        if (localScore ==0f) return; 
        //Debug.Log("Local High Score : "+ lvlName + " :"+ localScore);
        localScore = localScore / 100;
        localScore = Mathf.Round(localScore * 100.0f) * 0.01f;
        _localScore.text = localScore.ToString() + " s";

        //Debug.Log($"Comparison Lightning :{_lightningStar} Score:{localScore}");
        if(_lightningStar > localScore)
        {
            if (localScore == 0)
                return;
            _lightnightStarHidder.SetActive(false);
        }
    }

}
