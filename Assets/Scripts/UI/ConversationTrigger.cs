using DialogueEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConversationTrigger : MonoBehaviour
{
    [SerializeField] private NPCConversation currentConversation;
    private static ConversationTrigger instance;

    [SerializeField] private NextLevel _nextLevel;

    [SerializeField] private GameObject _controlsScreen;
  
    private void Awake()
    {
        currentConversation = GetComponent<NPCConversation>();
        
    }

   


    public void StartConversation()
    {
        ///Debug.Log(currentConversation.name);
        if (currentConversation != null)
        {
            //Cursor.lockState = CursorLockMode.None;
            ConversationManager.Instance.StartConversation(currentConversation);
            GameManager.StartDialogue(this.gameObject.GetComponent<ConversationTrigger>());
        }
        
    }


    public void NextOption()
    {
        //Debug.Log("NEXT OPTION");
        ConversationManager.Instance.SelectNextOption();
    }

    public void PreviousOption()
    {
        //Debug.Log("PREVIOUS OPTION");
        ConversationManager.Instance.SelectPreviousOption();

    }

    public void SelectOption()
    {
        //Debug.Log("SELECT  OPTION");
        ConversationManager.Instance.PressSelectedOption();

    }

    public void SkipDialog()
    {
        ConversationManager.Instance.EndConversation();

        if(_controlsScreen!=null)
            _controlsScreen.SetActive(false);

        if (_nextLevel != null)
        {
            _nextLevel.ShowScore();
            
        }
    }

}
