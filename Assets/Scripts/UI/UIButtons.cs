using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class UIButtons : MonoBehaviour
{
    [SerializeField] private GameObject _pauseMenu;
    [SerializeField] private GameObject _settings;
    [SerializeField] private GameObject _levelSelect;
    string _currentLevel = "LevelSelect";

    [SerializeField] private Slider InvertControlSlider;

    //FIRST OPTION Which Will be Selected 
    [SerializeField] private GameObject _pauseFirstSelected;
    [SerializeField] private GameObject _settingsFirstSelected; 
    

    private void Awake()
    {
        
    }
    private void Start()
    {
        if (_settings != null)
        {
            //_settings.SetActive(false);
            //Debug.Log("Setting False");
        }
        Scene currentScene = SceneManager.GetActiveScene();
        if (_levelSelect != null && currentScene.name != _currentLevel)
        {
            _levelSelect.SetActive(false);
        }
    }
    public void GoToLevelSelect()
    {
        _levelSelect.SetActive(true);
    }
    public void Settings()
    {
        _pauseMenu.SetActive(false);
        _settings.SetActive(true);

        if(_settingsFirstSelected != null)
            EventSystem.current.SetSelectedGameObject(_settingsFirstSelected);
    }
    public void BacktoPause()
    {
        _settings.SetActive(false);
        _pauseMenu.SetActive(true);

        if (_pauseFirstSelected != null)
            EventSystem.current.SetSelectedGameObject(_pauseFirstSelected);
    }
    public void StartGame(string startScene)
    {
        SceneManager.LoadScene(startScene);
        SetSlider();
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitToMenu(string mainMenu)
    {
        SceneManager.LoadScene(mainMenu);
    }

    public void Credits(string credits)
    {
        SceneManager.LoadScene(credits);
    }

    public void InvertControls()
    {
        GameManager.InvertControlsGM();
        SetSlider();

        //Restart();
    }

    private void SetSlider()
    {

        if (GameManager._isControlsInverted)
        {
            //Slider Value 1 
            InvertControlSlider.value = 1;
        }
        else
        {
            //Slider Value 0 
            InvertControlSlider.value = 0;
        }
    }

}
