using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGroup : MonoBehaviour
{
    public List<LevelButton> levelButtons;

    private void start()
    {
        Debug.Log("level button count: " + levelButtons.Count);
    }
    public void Subscribe(LevelButton btn)
    {
        if(levelButtons == null)
        {
            levelButtons = new List<LevelButton>();
        }
        levelButtons.Add(btn);
        Debug.Log("level button count: " + levelButtons.Count);
    }

}
