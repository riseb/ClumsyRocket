using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScrollToButton : MonoBehaviour
{
    public ScrollRect scrollRect; // Reference to the ScrollRect component
    public RectTransform contentPanel; // The content panel inside the ScrollRect
    private RectTransform selectedPanel; // Currently selected panel

    // This function will be triggered by Unity's Event System when a panel is selected
    public void OnPanelSelected(BaseEventData data)
    {
        // Cast the event data to the specific event type
        PointerEventData pointerData = (PointerEventData)data;

        // Get the RectTransform of the selected object (panel)
        RectTransform panelRect = pointerData.pointerEnter.GetComponent<RectTransform>();

        // Scroll to the selected panel
        AutoScrollTo(panelRect);
    }

    // Function to auto-scroll the scroll rect to the selected panel
    private void AutoScrollTo(RectTransform panelRect)
    {
        // Get the position of the selected panel relative to the content's position
        Vector3[] corners = new Vector3[4];
        panelRect.GetWorldCorners(corners);

        // Convert the world position of the corners to local position in the content panel
        Vector3 panelLocalPos = contentPanel.InverseTransformPoint(corners[0]);

        // Get the current scroll position of the content
        float contentHeight = contentPanel.rect.height;
        float viewportHeight = scrollRect.viewport.rect.height;

        // Calculate the offset of the selected panel relative to the content
        float offset = -panelLocalPos.y;

        // Normalize the position to scroll
        float normalizedPosition = Mathf.InverseLerp(0, contentHeight - viewportHeight, offset);

        // Set the vertical scroll position
        scrollRect.verticalNormalizedPosition = Mathf.Clamp01(normalizedPosition);
    }
}