using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

public class Scroll : MonoBehaviour
{
    [SerializeField] int maxPage;
    int currentPage;
    Vector3 targetPos;
    [SerializeField] Vector3 pageStep;
    [SerializeField] RectTransform levelPagesRect;
    [SerializeField] float tweenTime;
    [SerializeField] LeanTweenType tweenType;

    [SerializeField] GameObject[] PageActive;

    [SerializeField] TextMeshProUGUI chapterText;
    private void Awake()
    {
        currentPage = 1;
        targetPos = levelPagesRect.localPosition;


        SetPageActive();
    }
    public void Next()
    {
        if (currentPage < maxPage)
        {
            currentPage++;
            targetPos += pageStep;
            chapterText.text = ("Chapter " + (currentPage)).ToString();
            MovePage();
        }
    }
    public void Previous()
    {
        if (currentPage > 1)
        {
            currentPage--;
            targetPos -= pageStep;
            chapterText.text = ("Chapter " + (currentPage)).ToString();
            MovePage();
        }
    }
    void MovePage()
    {
        levelPagesRect.LeanMoveLocal(targetPos, tweenTime).setEase(tweenType);
        SetPageActive();
    }


    void SetPageActive()
    {
        foreach (var page in PageActive)
            page.SetActive(false);

        PageActive[currentPage - 1].SetActive(true);

    }
}
