using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static Cinemachine.DocumentationSortingAttribute;
using TMPro;


public class StartMenu : MonoBehaviour
{

    [Header ("PAGE REFERENCE")]
    [SerializeField] GameObject _levelSelectMenu;
    [SerializeField] GameObject _startMenu;
    [SerializeField] GameObject _creditMenu;
    [SerializeField] GameObject _SettingsMenu;
    [SerializeField] GameObject _SkinsMenu;
    [SerializeField] GameObject _ExitScreen;

    [Header("First Item selected")]
    [SerializeField] GameObject _levelMenuFirst;
    [SerializeField] GameObject _startMenuFirst;
    [SerializeField] GameObject _creditMenuFirst;
    [SerializeField] GameObject _settingsMenuFirst;
    [SerializeField] GameObject _SkinsMenuFirst;
    [SerializeField] GameObject _ExitScreenFirst;

    [Header("LEVEL MENU LEVELS")]
    [SerializeField] LevelList _lvlList;
    [SerializeField] LevelList _lvlLeaderboardList;
    [SerializeField] private Dropdown _lvlDropDwn;

    [SerializeField] private GameObject _buttonPrefab;
    [SerializeField] private GameObject _lvlPagePrefab;
    [SerializeField] private GameObject _lvlPagePrefabParent;

    [Header("Button Reference")]
    //[SerializeField] private GameObject _startButton;
    [SerializeField] private GameObject _continueButton;
    [SerializeField] private GameObject _newGameButton;
    

    [SerializeField] private int _loadLevelIndex;
    private int _maxLevels;

    public AudioClip hoverSFX;
    public AudioClip clickSFX;

    private void Awake()
    {
        if (!PlayerPrefs.HasKey("LastLevelIndex"))
        {
            _continueButton.SetActive(false);
        }
        else
        {
            _startMenuFirst = _continueButton;
            _newGameButton.SetActive(false);
        }
    }

    void Start()
    {
        Invoke("BackToStart", 0.1f);
        InstantiateLevels();
    }

    public void StartGame()
    {
        // FOR STARTING THE GAME 
        Debug.Log(" INSERT CODE FOR LAST PLAYED LEVEL");
        GameManager.Nextlevel("L 0");


    }

    public void ContinueGame()
    {
        int levelIndex = PlayerPrefs.GetInt("LastLevelIndex");
        Debug.Log("Level Index :"+ levelIndex);
        GameManager.Nextlevel(_lvlList.LevelName[levelIndex]);
    }
    public void SettingsButton()
    {
        _creditMenu.SetActive(false);
        _levelSelectMenu.SetActive(false);
        _startMenu.SetActive(false);
        _SettingsMenu.SetActive(true);
        _ExitScreen.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        if (_settingsMenuFirst != null)
            EventSystem.current.SetSelectedGameObject(_settingsMenuFirst);
    }

    public void BackToStart()
    {
        _creditMenu.SetActive(false);
        _levelSelectMenu.SetActive(false);
        _startMenu.SetActive(true);
        _SettingsMenu.SetActive(false);
        _SkinsMenu.SetActive(false);
        _ExitScreen.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        if (_startMenuFirst!=null)
            EventSystem.current.SetSelectedGameObject(_startMenuFirst);
    }

    public void ExitScreen()
    {
        _creditMenu.SetActive(false);
        _levelSelectMenu.SetActive(false);
        _startMenu.SetActive(false);
        _SettingsMenu.SetActive(false);
        _SkinsMenu.SetActive(false);
        _ExitScreen.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        if (_ExitScreenFirst != null)
            EventSystem.current.SetSelectedGameObject(_ExitScreenFirst);
    }

    public void LevelSelect()
    {
        // FOR SELECTING THE LEVEL 
        _levelSelectMenu.SetActive(true);
        _startMenu.SetActive(false);
        _SettingsMenu.SetActive(false);
        _ExitScreen.SetActive(false);

        EventSystem.current.SetSelectedGameObject(null);
        if (_levelMenuFirst != null)
            EventSystem.current.SetSelectedGameObject(_levelMenuFirst);

        

    }

    public void SkinsMenuSelect()
    {
        _levelSelectMenu.SetActive(false);
        _startMenu.SetActive(false);
        _SettingsMenu.SetActive(false);
        _ExitScreen.SetActive(false);
        _SkinsMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        if (_SkinsMenuFirst != null)
            EventSystem.current.SetSelectedGameObject(_SkinsMenuFirst);
    }

    public void LoadLevel()
    {
        GameManager.Nextlevel(_lvlList.LevelName[_lvlDropDwn.value]); // this is not optimum but idk how to use dropdown at 5:00AM in the morning LOL, Plz change this code later Risab ~Risab

    }

    public void Credits()
    {
       
        GameManager.Nextlevel("Credits Level");

    }




    public void ExitGame()
    {
       
        Application.Quit();
    }

    private void InstantiateLevels()
    {
        _maxLevels = _lvlList.LevelName.Length;
        Debug.Log("MaxLevels :"+ _maxLevels);
        _lvlPagePrefab.SetActive(false);
        GameObject LevelPage = _lvlPagePrefab;

        
        _loadLevelIndex = 0;


        for (int i = 0; i < _maxLevels -1 ;i++)
        {

            string nextLevel = _lvlList.LevelName[i];

            // CREATE A NEW PAGE 
            
            {
                LevelPage = Instantiate(_lvlPagePrefab, _lvlPagePrefabParent.transform);
                LevelPage.SetActive(true);
            }

            //CREATE BUTTONS 
            GameObject newButton = Instantiate(_buttonPrefab, LevelPage.transform);
            LevelButton buttonScript = newButton.GetComponent<LevelButton>();
            //   newButton.GetComponentInChildren<TextMeshProUGUI>().text = _lvlList.LevelName[i];
            buttonScript.loadLevel = _lvlList.LevelName[i];
            //buttonScript.levelLeaderboard = _lvlList.SteamLeaderboardName[i]; // THIS WILL SET THE LEADERBOARD // 
            buttonScript.StarsInit(_lvlList.LevelCompleteData[i]._TimeStar4, _lvlList.LevelCompleteData[i]._TimeStar3, _lvlList.LevelCompleteData[i]._TimeStar2);
            buttonScript._lvlImage.sprite = _lvlList.LevelCompleteData[i]._image;
            buttonScript.LoadLeaderboard(_lvlList.LevelName[i]);
            buttonScript.LocalHighScore(_lvlList.LevelName[i]);
            buttonScript._levelName.text = _lvlList.LevelName[i]; 
            //Button btn = newButton.GetComponent<Button>();

            //btn.onClick.AddListener(() => TestFunc(nextLevel));


            if(i ==0 )
            {
                _levelMenuFirst = newButton;
            }
        }
        _buttonPrefab.SetActive(false);
    }

    private void TestFunc(string lvl)
    {
        //Debug.Log(lvl);
        GameManager.Nextlevel(lvl);
    }

    //This function is to start the level from the level select menu
    public void startlevel()
    {
        GameManager.Nextlevel(_lvlList.LevelName[_loadLevelIndex]);

       
    }
    // this function is called in the Level Select menu when the Right arrow is clicked for changing the selected level
    public void LevelArrowRight()
    {
        if (_loadLevelIndex + 1 < _maxLevels -1 )
        {
            _loadLevelIndex++;
        }
            
    }
    // this function is called in the Level Select menu when the Left arrow is clicked for changing the selected level
    // Please change the Scroll Pages Numbers when adding more Levels 
    public void LevelArrowLeft()
    {
        if( _loadLevelIndex - 1 > -1)
        {
            _loadLevelIndex--;  
        }
            
    }


}
