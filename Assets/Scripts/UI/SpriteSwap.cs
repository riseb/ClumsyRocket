using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SpriteSwap : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, ISelectHandler, IDeselectHandler
{
    [Header("State Sprites")]
    [SerializeField]  private Sprite defaultImage;
    [SerializeField] private Sprite highlightedImage;
    [SerializeField] private Sprite pressedImage;
    [SerializeField] private Sprite selectedImage;

    private Image buttonImage;         // Reference to the button's image component
    private bool isSelected;

    void Awake()
    {
        buttonImage = GetComponent<Image>();

        // Initialize with the default image
        if (buttonImage != null && defaultImage != null)
        {
            buttonImage.sprite = defaultImage;
        }
    }

    void SetButtonImage(Sprite sprite)
    {
        if (buttonImage != null && sprite != null)
        {
            buttonImage.sprite = sprite;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!isSelected)
        {
            SetButtonImage(highlightedImage);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!isSelected)
        {
            SetButtonImage(defaultImage);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        SetButtonImage(pressedImage);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isSelected)
        {
            SetButtonImage(selectedImage);
        }
        else
        {
            SetButtonImage(highlightedImage);
        }
    }

    public void OnSelect(BaseEventData eventData)
    {
        isSelected = true;
        SetButtonImage(selectedImage);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        isSelected = false;
        SetButtonImage(defaultImage);
    }
}
