using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

using static LeanTween;
using System.Runtime.CompilerServices;



public class SettingsMenu : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer; // AM: audio Mixer

    [SerializeField] private Slider _mainSlider, _musicSlider, _sfxSlider, _vibrationSlider;

    [SerializeField] private TextMeshProUGUI _textResolution, _textGraphicsQuality, _textMusicChange;

    [SerializeField] private Toggle _fullscreenToggle, _RandomMusicToggle;

    //this is to notify that the settings have been applied

    [SerializeField] private GameObject notificationPanel;
    [SerializeField] private TextMeshProUGUI notificationText;
    [SerializeField] private float notificationDuration = 2f;
    [SerializeField] private float popupDuration = 0.5f;
    private Vector3 notifOriginalScale;

    //[SerializeField] private int notifOriginalSize = 24;
    //[SerializeField] private int notifTargetSize = 30;

    private float _newMusicVolume;
    private int _graphicsQualityIndex;
    private int _resolutionIndex;
    private int _MAXGRAPHICS = 3;
    private int _MAXRES;
    private int _musicIndex, _MAXMuscExist;

    private string[] _graphicQualityNames;
    private Resolution[] _resolutions;
    private List<Resolution> _resolutionsList;
    List<string> resolutionListString;
    public Resolution currentRes;

    private SoundE[] thisSoundE; //For storing the Names of the SoundTrack



    private void Start()
    {
        if(notificationPanel != null)
        {
            notificationPanel.SetActive(false);
            notifOriginalScale = notificationPanel.transform.localScale;
        }
         // Store the original scale
        
        // SETS THE VOLUME TO DEFAULT OR FIRST TIME 
        if (PlayerPrefs.HasKey("MasterVolSave"))
        {
            //Debug.Log("LOADING VOLUME ");
            LoadVolume();
        }
        else
        {
            // This will execute only once when the game is loaded 
            _audioMixer.SetFloat("MasterVolume", Mathf.Log10(_mainSlider.value) * 20);
            _audioMixer.SetFloat("MusicVolume", Mathf.Log10(_musicSlider.value) * 20);
            _audioMixer.SetFloat("SFXVolume", Mathf.Log10(_sfxSlider.value) * 20);

        }

        //SET THE Graphics Quality TO PREVIOUS OR NEW GAME
        //
        _graphicQualityNames = QualitySettings.names;

        if (PlayerPrefs.HasKey("GraphicsQualitySave"))
        {
            _graphicsQualityIndex = PlayerPrefs.GetInt("GraphicsQualitySave");
        }
        else
        {
            _graphicsQualityIndex = 2;
        }
        updateGraphicQuality();
        ResolutionInit();


        //SETTING THE FULLSCREENTOGGLE 
        if (PlayerPrefs.HasKey("FullscreenSave"))
        {
            if (PlayerPrefs.GetInt("FullscreenSave") == 1)
            {
                _fullscreenToggle.isOn = true;
            }
            else
            {
                _fullscreenToggle.isOn = false;
            }
        }
        else
        {
            _fullscreenToggle.isOn = true;
        }

        if (PlayerPrefs.HasKey("ControllerVibration"))
        {
            _vibrationSlider.value = PlayerPrefs.GetFloat("ControllerVibration");
            RumbleManager.RumbleSet(_vibrationSlider.value);
        }
        else
        {
            PlayerPrefs.SetFloat("ControllerVibration", 1);
            RumbleManager.RumbleSet(_vibrationSlider.value);
        }


        // UPDATE THE RANDOM MUSIC 
        Debug.Log("Random Music " + PlayerPrefs.GetInt("RandomMusicToggle"));

        if (PlayerPrefs.HasKey("RandomMusicToggle"))
        {

            if(PlayerPrefs.GetInt("RandomMusicToggle")== 1 )
            {
                _RandomMusicToggle.isOn = true;
            }
            else
            {
                _RandomMusicToggle.isOn = false;
            }
        }
        else
        {
            _RandomMusicToggle.isOn = false;
        }

        setMusic();
    }


    public void LoadVolume() // function to load the volume from previously set volume 
    {
        // This function will Load the Volume from the playePrefs //
        float masterVolume, musicVolume, sfxVolume;

        masterVolume = PlayerPrefs.GetFloat("MasterVolSave");
        musicVolume = PlayerPrefs.GetFloat("MusicVolSave");
        sfxVolume = PlayerPrefs.GetFloat("SFXVolSave");

        _mainSlider.value = masterVolume;
        _musicSlider.value = musicVolume;
        _sfxSlider.value = sfxVolume;

        _audioMixer.SetFloat("MasterVolume", Mathf.Log10(masterVolume) * 20);
        _audioMixer.SetFloat("MusicVolume", Mathf.Log10(musicVolume) * 20);
        _audioMixer.SetFloat("SFXVolume", Mathf.Log10(sfxVolume) * 20);

    }


    public void ApplyChangesButton()// this function will apply all changes made 
    {
        float masterVolume, musicVolume, sfxVolume;

        masterVolume = _mainSlider.value;
        musicVolume = _musicSlider.value;
        sfxVolume = _sfxSlider.value;

        //SETTING VOLUME 
        _audioMixer.SetFloat("MasterVolume", Mathf.Log10(masterVolume) * 20);
        _audioMixer.SetFloat("MusicVolume", Mathf.Log10(musicVolume) * 20);
        _audioMixer.SetFloat("SFXVolume", Mathf.Log10(sfxVolume) * 20);

        // SETTING THE PLAYERPREFS 
        PlayerPrefs.SetFloat("MasterVolSave", masterVolume);
        PlayerPrefs.SetFloat("MusicVolSave", musicVolume);
        PlayerPrefs.SetFloat("SFXVolSave", sfxVolume);
        PlayerPrefs.SetInt("GraphicsQualitySave", _graphicsQualityIndex);
        PlayerPrefs.SetInt("ResIndexSave", _resolutionIndex);
        PlayerPrefs.SetInt("FullscreenSave", _fullscreenToggle.isOn ? 1 : 0);
        PlayerPrefs.SetFloat("ControllerVibration", _vibrationSlider.value);
        PlayerPrefs.SetInt("RandomMusicToggle", _RandomMusicToggle.isOn ? 1 : 0);
        RumbleManager.RumbleSet(_vibrationSlider.value);

        //SETS THE GRAPHHICS QUALITY 
        QualitySettings.SetQualityLevel(_graphicsQualityIndex);

        RandomMusicToggle();

        //APPLY FULLSCREEN AND RESOLUTION// 
        ApplyResolution();


        Debug.Log("Changes Applied ");

    }

    public void ResolutionInit()
    {
        _resolutions = Screen.resolutions;
        _resolutionsList = new List<Resolution>();
        resolutionListString = new List<string>();
        string newRes; 
        foreach (Resolution res in _resolutions)
        {
            newRes = res.width.ToString() +" x " + res.height.ToString();
            if(!resolutionListString.Contains(newRes))
            {
                resolutionListString.Add(newRes);
                _resolutionsList.Add(res);
            }
            

        }

        _MAXRES = _resolutionsList.Count;
        _resolutionIndex = _MAXRES - 1;
        UpdateResolutionText();
    }

    public void ApplyResolution()
    {
        //Screen.fullScreen = _fullscreenToggle.isOn;

        Screen.SetResolution(_resolutionsList[_resolutionIndex].width,
            _resolutionsList[_resolutionIndex].height, _fullscreenToggle.isOn
            );
        

    }

    public void ReolutionRightButton()// Exactly as the name sounds
    {
        
        if (_resolutionIndex + 1 < _MAXRES)
        {
            _resolutionIndex++;
            UpdateResolutionText();
        }

    }

    public void ReolutionLeftButton()// Exactly as the name sounds
    {
        if (_resolutionIndex - 1 >= 0)
        {
            _resolutionIndex--;
            UpdateResolutionText();
        }

    }
    //This update is for the Text 
    public void UpdateResolutionText()
    {

       // resolutionsList[_resolutionIndex].ToString();
        //currentRes = Screen.currentResolution;

       // string height, width, hz; 
       // height = currentRes.height.ToString();
        //width = currentRes.width.ToString();
       // hz = currentRes.refreshRateRatio.ToString();
       // hz= hz.Substring(0, 4);
       // _textResolution.text = width +" x " + height + " " + hz + "hz";
       
        _textResolution.text = resolutionListString[_resolutionIndex].ToString(); 
    }

    public void QualityRightButton() // Exactly as the name sounds 
    {

        if (_graphicsQualityIndex + 1 < _MAXGRAPHICS)
        {
            _graphicsQualityIndex++;
            updateGraphicQuality();
        }

    }

    public void QualityLeftButton()// Exactly as the name sounds
    {

        if (_graphicsQualityIndex - 1 >= 0)
        {
            _graphicsQualityIndex--;
            updateGraphicQuality();
        }


    }

    private void updateGraphicQuality()
    {
        string str = _graphicQualityNames[_graphicsQualityIndex];
        string newstr = "";

        foreach (char c in str)
        {
            newstr += c;

        }

        //Debug.Log("printed maal is :"+newstr);
        _textGraphicsQuality.text = newstr;
    }

    public void SetVibration()
    {
        float vibration;
        vibration = _vibrationSlider.value;
        RumbleManager.RumbleSet(vibration);
        PlayerPrefs.SetFloat("ControllerVibration", _vibrationSlider.value);
    }

    public void SetAudio() // Sets the Volume levels of the Game on Slider Move
    {
        float masterVolume, musicVolume, sfxVolume;

        masterVolume = _mainSlider.value;
        musicVolume = _musicSlider.value;
        sfxVolume = _sfxSlider.value;

        _audioMixer.SetFloat("MasterVolume", Mathf.Log10(masterVolume) * 20);
        _audioMixer.SetFloat("MusicVolume", Mathf.Log10(musicVolume) * 20);
        _audioMixer.SetFloat("SFXVolume", Mathf.Log10(sfxVolume) * 20);


        PlayerPrefs.SetFloat("MasterVolSave", masterVolume);
        PlayerPrefs.SetFloat("MusicVolSave", musicVolume);
        PlayerPrefs.SetFloat("SFXVolSave", sfxVolume);

    }
    private void setMusic() // Initialising the Music 
    {
        thisSoundE = AudioManager.Instance.musicSound;
        int audioLenght = AudioManager.Instance.musicSound.Length;

        _MAXMuscExist = audioLenght; // Sets the Max number of music we have available 

        if (PlayerPrefs.HasKey("MusicIndex"))
        {
            _musicIndex = PlayerPrefs.GetInt("MusicIndex");
        }
        else
        {
            _musicIndex = 0;
            updateMusic();
        }
            

        if(PlayerPrefs.HasKey("RandomMusicToggle"))
        {
            if(PlayerPrefs.GetInt("RandomMusicToggle") == 1)
            {
                RandomMusicToggle();
            }
            else
            {
                updateMusic();
            }
        }
        else
        {
            // FIRST TIME CODE FOR RANDOM MUSIC TOGGLE
            PlayerPrefs.SetInt("RandomMusicToggle", 1);
            updateMusic();
        }
        
    }

    public void MusicleftButton()
    {
        if (_musicIndex - 1 >= 0 )
        {
            _musicIndex--;
            updateMusic();
            Debug.Log("Music Index : " +_musicIndex+"/"+_MAXMuscExist);
        }
    }

    public void MusicRightButton()
    {
        if (_musicIndex + 1 < _MAXMuscExist)
        {
            _musicIndex++;
            updateMusic();
            Debug.Log("Music Index : " + _musicIndex + "/" + _MAXMuscExist);
        }
    }

    private void updateMusic() // This is for the regular music ( no loop nonsense ) // On button Press 
    {
        string str;
        thisSoundE = AudioManager.Instance.musicSound;
        if (thisSoundE != null)
        {
            str = thisSoundE[_musicIndex]._name;
            _textMusicChange.text = str;
        }
        Debug.Log("Random Music : "  +_RandomMusicToggle.isOn);

        if(!_RandomMusicToggle.isOn)
        {
            AudioManager.Instance.PlayMusic(thisSoundE[_musicIndex]._name);
            PlayerPrefs.SetInt("MusicIndex", _musicIndex);
        }
        else
        {
            // Do nothing because the random music is playing 
        }
        
    }

    private void RandomMusicToggle()
    {
        if(PlayerPrefs.GetInt("RandomMusicToggle") == 1)
        {
            AudioManager.Instance.CancelInvoke();
            AudioManager.Instance.PlayRandomMusic();
        }
        else
        {
            AudioManager.Instance.CancelInvoke();
        }
    }


    public void ShowNotification()
    {
        if(notificationPanel != null)
        {
            notificationPanel.SetActive(true);
            scale(notificationPanel, notifOriginalScale, popupDuration).setEase(LeanTweenType.easeOutBack);

            Invoke("HideNotification", notificationDuration);
        }
       
    }
    private void HideNotification()
    {
        if(notificationPanel != null)
        {
            notificationPanel.SetActive(false);
        }
        
    }


}
