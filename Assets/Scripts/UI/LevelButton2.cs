
#if !(UNITY_STANDALONE_WIN || UNITY_STANDALONE_LINUX || UNITY_STANDALONE_OSX || STEAMWORKS_WIN || STEAMWORKS_LIN_OSX)
#define DISABLESTEAMWORKS
#endif

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using TMPro;
using System;

#if !DISABLESTEAMWORKS
using Steamworks;
#endif


public class LevelButton2 : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, ISelectHandler, IDeselectHandler
{
    [SerializeField] public int _levelIndex;

    // Local Information 
    public string LevelNameDisplaySTR;
    public Sprite _thislevelSprite;
    public string levelLeaderboard;
    public string highScoreSTR;

    #if !DISABLESTEAMWORKS
    //Steamworks Leaderboard Information
    private string s_leaderboardName;
    private SteamLeaderboard_t s_currentLeaderboard;
    private bool s_initialized = false;
    private CallResult<LeaderboardFindResult_t> m_findResult = new CallResult<LeaderboardFindResult_t>();
    private CallResult<LeaderboardScoreUploaded_t> m_uploadResult = new CallResult<LeaderboardScoreUploaded_t>();
    private CallResult<LeaderboardScoresDownloaded_t> m_downloadResult = new CallResult<LeaderboardScoresDownloaded_t>();
    
    private LeaderboardEntry_t[] _leaderboardEntry_localinfo; // This will save the entry of the leaderboard associated with the current button  
#endif
    [Header("LeaderBoard UI Elements")]
    [SerializeField] List<TextMeshProUGUI> _leaderboardRanks = new List<TextMeshProUGUI>();
    [SerializeField] List<TextMeshProUGUI> _leaderboardName = new List<TextMeshProUGUI>();
    [SerializeField] List<TextMeshProUGUI> _leaderboardScore = new List<TextMeshProUGUI>();
    [SerializeField] TextMeshProUGUI _localScore;
    [SerializeField] public TextMeshProUGUI _levelName;
    [SerializeField] public GameObject _lightnightStarHidder;
    [SerializeField] TextMeshProUGUI _thisLevelIconNum;
    [SerializeField] TextMeshProUGUI _NumPollaroidTxt;

    [Header("Image")]
    [SerializeField] public Image _lvlImage;


    [Header("LEVEL MENU LEVELS")]
    [SerializeField] private TextMeshProUGUI star1Time;
    [SerializeField] private TextMeshProUGUI star2Time;
    [SerializeField] private TextMeshProUGUI star3Time;


    [SerializeField] LevelList _levelList;

    private float _lightningStar;

    private void Awake()
    {
       
            int levelunlockeddebug = PlayerPrefs.GetInt("LevelUnlocked");
        

        if (!PlayerPrefs.HasKey("LevelUnlocked"))
            PlayerPrefs.SetInt("LevelUnlocked", 0);


        if (PlayerPrefs.HasKey("LevelUnlocked"))
        {
            if (PlayerPrefs.GetInt("LevelUnlocked") >= _levelIndex)
            {
                // CODE FOR LOCKING THE LEVEL 
                gameObject.SetActive(true);
                Debug.Log("Level " + _levelIndex + "Active");
            }
            else
            {
                gameObject.SetActive(false);
                Debug.Log("Level " + _levelIndex + "unactive");
            }

        }


    }
    private void Start()
    {

        #if !DISABLESTEAMWORKS

        if (SteamManager.Initialized)
        {
            string name = SteamFriends.GetPersonaName();
        }
        //LevelNameDisplaySTR = _levelList.LevelName[_levelIndex]; 
        LoadLeaderboard(_levelList.LevelName[_levelIndex]);
        _thislevelSprite = _levelList.LevelCompleteData[_levelIndex]._image;

        LocalHighScore(_levelList.LevelName[_levelIndex]);
        //Debug.Log("Loading Leaderboard : " + _levelList.LevelName[_levelIndex]);
        _thisLevelIconNum.text = (_levelIndex + 1).ToString();
        #endif
    }
#if !DISABLESTEAMWORKS
    public void LoadLeaderboard(string leaderboard)
    {
        s_leaderboardName = leaderboard;
        GetLeaderboard();
    }


    public void GetLeaderboard()
    {

        if (SteamManager.Initialized)
        {
            SteamAPICall_t hSteamAPICall = SteamUserStats.FindLeaderboard(s_leaderboardName);
            m_findResult.Set(hSteamAPICall, GettingLeaderboard);
        }

    }
    public void GettingLeaderboard(LeaderboardFindResult_t pCallback, bool failure)
    {
        Debug.Log($"Steam Leaderboard Find: Did it fail? {failure}, Found: {pCallback.m_bLeaderboardFound}, leaderboardID: {pCallback.m_hSteamLeaderboard.m_SteamLeaderboard} ");
        s_currentLeaderboard = pCallback.m_hSteamLeaderboard;
        s_initialized = true;
        SteamAPICall_t hSteamAPICall = SteamUserStats.DownloadLeaderboardEntries(s_currentLeaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal, 1, 10);
        m_downloadResult.Set(hSteamAPICall, GetScore);
    }
    public void GetScore(LeaderboardScoresDownloaded_t pCallback, bool failure)
    {
        string scorestr;
        float displayscore;
        if (!failure)
        {
           
            int maxEntry = (pCallback.m_cEntryCount < 7 ? pCallback.m_cEntryCount : 7);
            Debug.Log( "Entry :" +maxEntry);

            for (int i = 0; i < pCallback.m_cEntryCount; i++)
            {
                LeaderboardEntry_t entry;
                SteamUserStats.GetDownloadedLeaderboardEntry(pCallback.m_hSteamLeaderboardEntries, i, out entry, null, 0);
                

                if (true) // Make this true if you want to use the old cole 
                {
                    if (_leaderboardRanks[i] != null)
                        _leaderboardRanks[i].text = entry.m_nGlobalRank + ". ";

                    if (_leaderboardName[i] != null)
                        _leaderboardName[i].text = SteamFriends.GetFriendPersonaName(entry.m_steamIDUser).ToString();

                    //this code will need to have the conversion of the score
                    if (_leaderboardScore[i] != null)
                    {
                        displayscore = entry.m_nScore;
                        displayscore = displayscore / 100;
                        scorestr = displayscore.ToString() + " s";
                        _leaderboardScore[i].text = scorestr;
                    }
                }
            }

        }
        else
            Debug.Log("Failed to Retreive Leaderbaord Scores ");
    }

#endif
    public void OnPointerEnter(PointerEventData eventData)
    {
        
        ShowData();
        

    }

    public void OnPointerExit(PointerEventData eventData)
    {
       
        HideData();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
       
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        
    }

    public void OnSelect(BaseEventData eventData)
    {
        // CODE TO LOAD THE LEVEL INFORMAION 
        ShowData();
        //LoadLeaderboard(_levelList.LevelName[_levelIndex]);
        
    }

    public void OnDeselect(BaseEventData eventData)
    {
        // CODE TO UNLOAD THE LEVEL INFORMATION 
        HideData();
    }



    private void ShowData()
    {
        #if !DISABLESTEAMWORKS
        LoadLeaderboard(_levelList.LevelName[_levelIndex]); // Load the Leaderboard 

        _levelName.text = LevelNameDisplaySTR;// Change the Level Name 

        _lvlImage.sprite = _thislevelSprite;

        _localScore.text = highScoreSTR;

        _NumPollaroidTxt.text = ("#"+ (_levelIndex + 1)).ToString();
#endif

    }

    private void HideData()
    {
        // Clear the Leaderboar So No New Data is generated // 

        foreach (TextMeshProUGUI text in _leaderboardName) text.text = "";
        foreach (TextMeshProUGUI text in _leaderboardRanks) text.text = "";
        foreach (TextMeshProUGUI text in _leaderboardScore) text.text = "";

    }

    public void LoadLevel() // Used to Load the Level // 
    {
        GameManager.Nextlevel(_levelList.LevelName[_levelIndex]);
    }

    public void LocalHighScore(string lvlName)
    {

        float localScore = PlayerPrefs.GetFloat(lvlName);

        if (localScore != 0f)
        {
            //Debug.Log("Local High Score : "+ lvlName + " :"+ localScore);
            localScore = localScore / 100;
            localScore = Mathf.Round(localScore * 100.0f) * 0.01f;
            _localScore.text = localScore.ToString() + " s";

            highScoreSTR = localScore.ToString() + " s";
        }
        else
        {
            highScoreSTR = "N/A";

        }
        
        
        //Debug.Log($"Comparison Lightning :{_lightningStar} Score:{localScore}");
        if (_lightningStar > localScore)
        {
            if (localScore == 0)
                return;
            _lightnightStarHidder.SetActive(false);
        }
    }

}
