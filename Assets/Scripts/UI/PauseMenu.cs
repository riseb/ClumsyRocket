using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{

    [SerializeField] GameObject _settingsMenu;

    [SerializeField] GameObject _settingsFirstSelected;
    [SerializeField] GameObject _pauseFirstSelected;

    [SerializeField] private Slider InvertControlSlider;


    public void ResumeGameButton()
    {
        GameManager.PauseGame();
    }

    public void FlipControlsButton()
    {
        GameManager.InvertControlsGM();
        SetSlider();
    }

    
    public void SettingsButton()
    {
        _settingsMenu.SetActive(true);

        if(_settingsFirstSelected!=null)
            EventSystem.current.SetSelectedGameObject(_settingsFirstSelected);
    }

    public void RestartButton()
    {
        GameManager.Restart();
    }

    public void QuitToMenuButton()
    {
        //INSERT CODE TO QUIT MENU 
        Debug.Log("No Code for Quitting the Game ");
        
        GameManager.Nextlevel("StartScreen");
        GameManager.PauseGame();

    }


    public void SettingsBack()
    {
        _settingsMenu.SetActive(false);
        

        if (_pauseFirstSelected != null)
            EventSystem.current.SetSelectedGameObject(_pauseFirstSelected);
    }




    //this function is for Flip controls Button Slider 
    private void SetSlider()
    {

        if (GameManager._isControlsInverted)
        {
            //Slider Value 1 
            InvertControlSlider.value = 1;
        }
        else
        {
            //Slider Value 0 
            InvertControlSlider.value = 0;
        }
    }

}
