using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableWall : MonoBehaviour
{
    [SerializeField] BoxCollider _bc; 


    // Start is called before the first frame update
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Bullet")
        {
            // Code for breaking the wall 
            Destroy(collision.gameObject);
            //Destroy(this.gameObject);
            if (_bc != null)
            {
                _bc.enabled = false;
            }
            else
                Debug.LogError("ADD REFERENCE FOR BOX COLLDIER ");


            AddRigidbodyRecursively(transform);



        }
    }


    void AddRigidbodyRecursively(Transform parentTransform)
    {
        // Iterate through all child transforms of the current parent
        foreach (Transform child in parentTransform)
        {
            // Check if the child GameObject already has a Rigidbody component
            Rigidbody rb = child.GetComponent<Rigidbody>();

            // If Rigidbody component doesn't exist, add it
            if (rb == null)
            {
                rb = child.gameObject.AddComponent<Rigidbody>();
                rb.constraints = RigidbodyConstraints.FreezePositionZ;
                // You can customize the Rigidbody properties here if needed
                // Example: rb.mass = 1.0f;
            }

            // Recursively call this method for each child to handle nested children
            AddRigidbodyRecursively(child);
        }
    }
}
