using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BulletEnemy : MonoBehaviour
{
    [SerializeField] public float _bulletSpeed = 10f;
    [SerializeField] public float _rotationSpeed = 10f;
    [SerializeField] GameObject _missileMesh;
    private Vector3 _dir;
    private Rigidbody _rb;
    [SerializeField] private ParticleSystem _vfx_Explosion;

    private int _extraForce;

    private GameObject _player;

    public UnityEvent onDeath;

    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _dir = transform.up;
        _rb.velocity = _dir * _bulletSpeed;

        _player = GameObject.FindGameObjectWithTag("Player");
    }

    private void FixedUpdate()
    {
        _dir = _player.transform.position - transform.position;

        _missileMesh.transform.LookAt(_player.transform);

         Vector3 neededForce = _rb.mass * (_dir.normalized * _bulletSpeed - _rb.velocity) / Time.fixedDeltaTime;
        
        _rb.AddForce(neededForce);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (_vfx_Explosion != null)
            {
                PlayerController _playerScript = collision.gameObject.GetComponent<PlayerController>();
                _playerScript.Death();

            }
            disableMisile();


        }
        else
        {
            disableMisile();
        }



    }

    void disableMisile()
    {
        ParticleSystem newParticle = Instantiate(_vfx_Explosion,transform.position, Quaternion.identity);
        newParticle.Play();

        //_vfx_Explosion.Play();
        this.gameObject.SetActive(false);

        onDeath?.Invoke();
    }

}
