using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    [SerializeField] public float _bulletSpeed = 10f;
    private Vector3 _dir;
    private Rigidbody _rb;
    [SerializeField] private ParticleSystem _vfx_Explosion;

    private int _extraForce;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _dir = transform.up;
        _rb.velocity = _dir* _bulletSpeed;
    }

    
    private void FixedUpdate()
    {
        Vector3 neededForce = _rb.mass * (_dir * _bulletSpeed - _rb.velocity) / Time.fixedDeltaTime;
        // Apply the force
        _rb.AddForce(neededForce);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
            return; 

        
        explode();
    }

    public void explode()
    {
        // CODE FOR EXPLOSION 

        Debug.Log("BULET ESPLOZION");

        if (_vfx_Explosion != null)
        {
            _vfx_Explosion.Play();
        }


        //Instantiate(_vfx_Explosion, this.transform.position);
        Instantiate(_vfx_Explosion, transform.position, transform.rotation);
        //this.gameObject.SetActive(false);

        Destroy(this.gameObject);


    }
}
