using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.EnhancedTouch;


[CreateAssetMenu(menuName = "ScriptableObject/Input Reader")]


public class InputReader :ScriptableObject, PlayerInput.IGameplayActions, PlayerInput.IUIActions, PlayerInput.IDialogActions 
{
    private PlayerInput _playerInput;


    private TouchControl _touchControls;
    private bool _rightSideTouch;
    float _screenWidth;
    private Vector2 _touch0Pos, _touch1Pos;
    private bool _touch0Right, _touch1Right; //True if this touch is controlling the Right Thruster

    //private bool _isPause;

    private void Awake()
    {
        _touchControls = new TouchControl();
    }

    private void OnEnable()
    {
        _screenWidth = Screen.width;

        if (_playerInput == null)
        {
            _playerInput = new PlayerInput();
            _playerInput.Gameplay.SetCallbacks(instance: this);
            _playerInput.UI.SetCallbacks(instance: this);
            _playerInput.Dialog.SetCallbacks(instance: this);
            //_isPause = false;
            SetGameplay();

        }

        
    }


    public void SetGameplay()
    {
        //Debug.Log("SETGAMEPLAY");

        _playerInput.Gameplay.Enable();
        _playerInput.UI.Disable();
        _playerInput.Dialog.Disable();
    }

    public void SetUI()
    {
        //Debug.Log("SETUI");

        _playerInput.Dialog.Disable();
        _playerInput.Gameplay.Disable();
        _playerInput.UI.Enable();
    }

    public void SetDialogue()
    {

        //Debug.Log("SET DIALOGE");

        _playerInput.Dialog.Enable();
        _playerInput.Gameplay.Disable();
        _playerInput.UI.Disable();
        
    }

    public event Action LeftThrusterEvent;
    public event Action RightThrusterEvent;
    public event Action LeftThrusterCancelledEvent;
    public event Action RightThrusterCancelledEvent;
    public event Action PauseEvent;
    public event Action RestartEvent;
    public event Action InteractAction;
    public event Action RestartCameraEvent;

    // Dialog 
    public event Action NextOptionEvent;
    public event Action PrevOptionEvent;
    public event Action SelectOptionEvent;
    public event Action SkipDialogEvent;

    public event Action BackButtonEvent;
    public event Action onApplyButton;


    // IMPLEMENTING INTERFACES ///


    public void OnLeftThruster(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Started)
        {
            LeftThrusterEvent?.Invoke();
        }
        if (context.phase == InputActionPhase.Canceled)
        {
            LeftThrusterCancelledEvent?.Invoke();
        }
    }

    public void OnRightThruster(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Started)
        {
            RightThrusterEvent?.Invoke();
        }
        if (context.phase == InputActionPhase.Canceled)
        {
            RightThrusterCancelledEvent?.Invoke();
        }
    }

    public void OnPause(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            
            PauseEvent?.Invoke();
        }
    }

    public void OnRestart(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            RestartCameraEvent?.Invoke();
            
        }

        if (context.phase == InputActionPhase.Canceled)
        {
            RestartEvent?.Invoke();

        }


    }

    public void OnResume(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            
            PauseEvent?.Invoke();
        }

     

    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        {
            InteractAction?.Invoke();
        }
    }

   

    

    public void OnNextDialog(InputAction.CallbackContext context)
    {
       
        if (context.phase == InputActionPhase.Performed)
        {
            SetUI();
            //Debug.Log("PAUSE");
            NextOptionEvent?.Invoke();
        }
    }

    public void OnNextOption(InputAction.CallbackContext context)
    {

        
        if (context.phase == InputActionPhase.Performed)
        {
            NextOptionEvent?.Invoke();
        }
    }

    public void OnPrevOption(InputAction.CallbackContext context)
    {
        
        if (context.phase == InputActionPhase.Performed)
        {
            PrevOptionEvent?.Invoke();

        }
    }

    public void OnSelectOption(InputAction.CallbackContext context)
    {
        
        if (context.phase == InputActionPhase.Canceled)
        {
            //Debug.Log("Tap");

            SelectOptionEvent?.Invoke();
        }
        if (context.phase == InputActionPhase.Performed)
        {
            //Debug.Log("HOLD");
            SkipDialogEvent?.Invoke();
        }
    }

    public void OnSkipDialog(InputAction.CallbackContext context)
    {
        
    }

    public void OnBack(InputAction.CallbackContext context)
    {
        if (context.phase == InputActionPhase.Performed)
        { BackButtonEvent?.Invoke(); 
        }
           
    }

    public void OnApply(InputAction.CallbackContext context)
    {
       
        if(context.phase == InputActionPhase.Performed)
        {
            onApplyButton?.Invoke();
        }
    }

   



    public void OnTouch0(InputAction.CallbackContext context)
    {
        

    }

    public void OnTouch1(InputAction.CallbackContext context)
    {

            

    }

    private bool ReturnRightSideBool(float touchLocation)
    {
        


        if (touchLocation < _screenWidth/2)
        {
            Debug.Log("Left SIDE TOUCH");
            return false;
        }

        else
        {
            Debug.Log("Right SIDE TOUCH");
            return true;
        }
            
    }

    public void LeftButtonDown()
    {
        LeftThrusterEvent?.Invoke();
       
    }

    public void RightButtonDown()
    {
        RightThrusterEvent?.Invoke();
    }
    public void LeftButtonUp()
    {
        LeftThrusterCancelledEvent?.Invoke();
       
    }

    public void RightButtonUp()
    {
        RightThrusterCancelledEvent?.Invoke();
    }


}
