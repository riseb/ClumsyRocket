using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftableObject : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private Material _M_Tractor;
    private Material _M_OGMAT;
    private  Renderer _thisBodyRender;

    [SerializeField] private GameObject _GlowBody; //This is the Outer thing That shall Glow 

    private GoalMarker _GoalMarker;

    [SerializeField] private GameObject _respawnLocation; 

    private void Start()
    {
        _thisBodyRender = GetComponent<Renderer>();

        // Store the original material
        _M_OGMAT = _thisBodyRender.material;

        _GlowBody.SetActive(false);


        //Make itself Known to the Objective Indicator 

        _GoalMarker = GameObject.FindGameObjectWithTag("Indicator").GetComponent<GoalMarker>();
        _GoalMarker.AddOBJtoList(this.gameObject);


    }
    public void ActivateGlow()
    {
        // CODE TO CHANGE COLOR TO SOMETHING ELSE 
        //_thisBodyRender.material = _M_Tractor;
        _GlowBody.SetActive(true);  
    }

    public void DeactivateGlow()
    {
        // CODE TO RETURN THE OBJECT TO ITS FORMER GLORY 
        //_thisBodyRender.material = _M_OGMAT;
        _GlowBody.SetActive(false);
    }


    public void DestroyNow()
    {
        _GoalMarker.RemoveOBJtoList(this.gameObject);
        _GoalMarker.SetBoxTarget();

        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(_GoalMarker !=null)
        {
            _GoalMarker.SetObjectivePlatfromTarget();
        }
        else
        {
            Debug.LogWarning(" ADD GOAL MARKER TO LEVEL DUMBASSS ");
        }

        if(other.gameObject.tag =="OutBound")
        {
            if(_respawnLocation != null)
                transform.position = _respawnLocation.transform.position;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (_GoalMarker != null)
        {
            _GoalMarker.SetBoxTarget();
        }
        else
        {
            Debug.LogWarning(" ADD GOAL MARKER TO LEVEL DUMBASSS ");
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "OutBound")
        {
            Debug.Log(" RESPAWN ");
                
            if (_respawnLocation != null)
                transform.position = _respawnLocation.transform.position;

            _GoalMarker.SetObjectivePlatfromTarget();
        }
    }
}
