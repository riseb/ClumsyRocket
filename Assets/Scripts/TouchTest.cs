using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class TouchTest : MonoBehaviour
{
    public Text PhaseDisplayText;
    private int maxTapCount = 10;
    private string multitouchInfo; 

    private Touch theTouch;
    private float timeTouchEnded;
    private float displayTime = 0.5f;

    // Update is called once per frame
    void Update()
    {

        multitouchInfo = string.Format("Max Tap count : {0}\n", maxTapCount);

        if(Input.touchCount > 0)
        {
            for (int i=0; i<Input.touchCount; i++)
            {
                theTouch = Input.GetTouch(i);

                multitouchInfo +=
                    string.Format("Touch {0} - Position {1} - Tap COunt: {2} - Finger {3} \n" +
                    "rRadius : {4} ({5}%) \n",
                    i,theTouch.position,theTouch.tapCount, theTouch.fingerId, theTouch.radius,
                    ((theTouch.radius/(theTouch.radius +theTouch.radiusVariance))*100f).ToString("F1") );



                if(theTouch.tapCount > maxTapCount )
                {
                    maxTapCount = theTouch.tapCount;
                }

                
            }
        }

        PhaseDisplayText.text = multitouchInfo;
    }
}
