using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Collectible : MonoBehaviour
{
    private LevelManager _LVLM;

    [SerializeField] GameObject _particleEffect;

    [SerializeField] ParticleSystem _PSEffect;
    [SerializeField] GameObject _body; 

    [SerializeField] Vector3 _rotationSpeed = new Vector3(0, 100, 0);

    [SerializeField] string _unlockSkin;

    [SerializeField] GameObject newSkinText;


    public UnityEvent SkinCollectedEvent;

    void Update()
    {
        transform.Rotate(_rotationSpeed * Time.deltaTime);
    }
   



    private void Awake()
    {
        
    }

    private void NewSkinCollectedText()
    {
        newSkinText.SetActive(true);
        
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerPrefs.SetInt(_unlockSkin, 1);

            NewSkinCollectedText();

            if(SkinCollectedEvent != null ) 
                SkinCollectedEvent.Invoke();


            if (_particleEffect != null)
            {
                Instantiate(_particleEffect, this.transform.position, Quaternion.identity);
            }
            Destroy(gameObject);
        }


        
    }
}
