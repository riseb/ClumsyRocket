using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AutoSelectButton : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnEnable()
    {

        Invoke("SelectButton", 0.1f);



    }

    private void SelectButton()
    {
        GameObject button = GameObject.FindGameObjectWithTag("Button");
        Debug.Log(" Level Menu Loaded  ");
        if (button != null)
        {
            
            EventSystem.current.SetSelectedGameObject(button);


        }
    }
}
