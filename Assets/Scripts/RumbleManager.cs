using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RumbleManager : MonoBehaviour
{
    public static RumbleManager instance;

    public static bool _isRumble;

    private Gamepad _pad; 

    [SerializeField] float _highFrequency = 1f, _lowFrequency = 0.5f;

    private void Awake()
    {
        _isRumble = false; 

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
            return;
        }
    }

    public static void RumbleSet(float frequency)
    {
        // Funciton Used by Settings to Change the Rumble settings 

        instance._highFrequency = frequency;


        instance._lowFrequency = instance._highFrequency / 2;

        //Debug.Log("Setting the Controller Frequency");

    }

    public void RumbleStart()
    {

        _pad = Gamepad.current;

        if(_pad != null)
        {
            if (!_isRumble)
            {
                if (GameManager._device > 0)
                {
                    _pad.SetMotorSpeeds(_lowFrequency, _highFrequency);
                    Debug.Log("Rumblll Deez nuts");


                }
                _isRumble = true;
            }
            
        }
    }

    public void RumbleStop()
    {
        if (_pad != null)
        {
            if (_isRumble)
            {
                if (GameManager._device > 0)
                {
                    _pad.SetMotorSpeeds(0, 0);

                }
                _isRumble = false;
                //Debug.Log("Dooonnnttt Rumbbbb deeeezz Nutssss");
            }
        }
    }


    public void RumblePulse()
    {
        if (_pad != null)
        {
            if (!_isRumble)
            {
                if (GameManager._device > 0)
                {
                    _pad.SetMotorSpeeds(_lowFrequency, _highFrequency);
                    Invoke("RumbleStop",0.5f);
                    //Debug.Log("Rumble Death");

                }
                _isRumble = true;
            }

        }
    }
}
