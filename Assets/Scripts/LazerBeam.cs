using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerBeam : MonoBehaviour
{

    private bool _isActive;

    [Tooltip("True if you want it to be ON-OFF , false for static off ")]
    [SerializeField] private bool _isDynamic = false; 

    [SerializeField] private float _activateFlipTimer = 3f; // Stay Activated for seconds 
    [SerializeField] private float _deactivateFlipTimer = 3f; // Stay deactivated for seconds 

    [SerializeField] private float _activateoffset = 1f;
    [SerializeField] GameObject lazerbeamMesh;
    [SerializeField] ParticleSystem _ps_LazerStart, _ps_LazerBeamm
        ;

    private void Start()
    {   
        
        _isActive = false;

        if(_isDynamic)
        {
            //InvokeRepeating("OnOff", _activateoffset, _activateFlipTimer);
            //InvokeRepeating("LazerActive", _activateFlipTimer + _activateoffset - _ps_LazerStart.main.duration, _activateFlipTimer);
            Invoke("OnLazer", _activateoffset);
        }
        else
        {
            _ps_LazerBeamm.Play();
            _isActive = true;
        }
    }

    private void OnEnable()
    {
        if(!_isDynamic )
        {
            _ps_LazerBeamm.Play();
        }
    }

    private void OnOff()
    {
        _isActive = !_isActive;
        //lazerbeamMesh.SetActive(_isActive);
        if(_isActive)
        {
            _ps_LazerBeamm.Play();
        }
        else
        {
            _ps_LazerBeamm.Stop();
        }
    }


    private void OnLazer()
    {
        _ps_LazerBeamm.Play();
        _isActive = true;
        Invoke("OffLazer", _activateFlipTimer);
    }

    private void OffLazer()
    {
        _ps_LazerBeamm.Stop();
        _isActive = false;
        Invoke("OnLazer", _deactivateFlipTimer);
        Invoke("LazerEffectStart", _deactivateFlipTimer - _ps_LazerStart.main.duration);
    }

    private void LazerEffectStart()
    {
        if (_ps_LazerStart != null)
        {
            _ps_LazerStart.Play();
        }
    }


    private void LazerActive()
    {
        if(_ps_LazerStart != null)
        {
            _ps_LazerStart.Play();
        }
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (_isActive)
            {
                PlayerController player = other.gameObject.GetComponent<PlayerController>();
                player.Death();
            }

        }
    }

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if(_isActive)
            {
                PlayerController player = other.gameObject.GetComponent<PlayerController>();
                player.Death();
            }
            
        }
    }

    public void OffLazerPermanent()
    {
        _ps_LazerBeamm.Stop();
        _isActive = false;
    }



}
