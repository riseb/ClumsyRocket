using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RocketGun : RocketAttatch
{

    [SerializeField] GameObject _bulletPFB;
    [SerializeField] Transform _muzzle;

    private Bullet _bullet;

    [SerializeField] GameObject _missileMesh; 

    [SerializeField] float _shootForce;

    [SerializeField] TextMeshPro _textMissileCount;

    [SerializeField] GameObject _player;
    private Rigidbody _playerRB;

    private bool _canShoot;

    [SerializeField] int _totalAmmo = 5;
    private int _ammo;
    [SerializeField] float _timeTillNextShot = 3f;

    private void Start()
    {
        _canShoot = true;
        _ammo = _totalAmmo;
        UpdateBulletCOunt();

        _playerRB = _player.GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    public override void Action()
    {
        base.Action();
        // Shooting Mechanics 
        if (_bulletPFB != null && _canShoot)
        {
            GameObject newBullet = Instantiate(_bulletPFB, _muzzle.transform.position, _muzzle.transform.rotation);
            _bullet = newBullet.GetComponent<Bullet>();
            _bullet._bulletSpeed = _bullet._bulletSpeed + Mathf.Abs(_playerRB.velocity.y);
            Rigidbody rb =newBullet.GetComponent<Rigidbody>();
            
            
            Debug.Log("Player velocity "+ _playerRB.velocity);
            //rb.AddForce(_shootForce * _muzzle.transform.up, ForceMode.Impulse);
            _canShoot = false;
            DeactivateRocketMesh();
            _ammo--;
            UpdateBulletCOunt();
            Invoke("PlayerCanShoot", _timeTillNextShot);
        }



    }


    void PlayerCanShoot()
    {
        if (_ammo > 0)
        {
            _canShoot = true;
            _missileMesh.SetActive(true);
        }
        
    }

    private void DeactivateRocketMesh()
    {
        if (_missileMesh != null)
        {
            _missileMesh.SetActive(false);

        }
    }

    private void UpdateBulletCOunt()
    {
       if(_ammo > 0)
        {
            _textMissileCount.text = _ammo.ToString();
        }

       if (_ammo == 0)
        {
            _textMissileCount.text = "-";
        }
    }
}
