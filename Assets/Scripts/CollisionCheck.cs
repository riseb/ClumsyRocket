using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCheck : MonoBehaviour
{
    GameObject _liftObject;
    Rigidbody _rbLiftObj;
    private bool _objectAttatch;
    FixedJoint _fj;

    private void Start()
    {
        _objectAttatch = false;
        
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "LiftableObject")
        {
            _liftObject = collision.gameObject;
            Debug.Log("Can LIFT ITEM");
            //ToggleMagnet();

        }

    }

    private void OnCollisionExit(Collision collision)
    {
        if (_liftObject != null)
        {
            Debug.Log("Object Not attatched anymore");

            _liftObject = null;
        }
    }


    public void ToggleMagnet()
    {
        if (_objectAttatch)
        {
            //When True 
            //_rbLiftObj.isKinematic = true;
            Destroy(_fj);
            //_fj.connectedBody = null;
            _objectAttatch = false;
        }
        else
        {
            // WHEN FALSE 
            if (_liftObject != null)
            {
                _rbLiftObj = _liftObject.GetComponent<Rigidbody>();

                

                _fj = _liftObject.AddComponent<FixedJoint>();
                _fj.connectedBody = this.gameObject.GetComponent<Rigidbody>();
                _objectAttatch = true;
            }
        }
    }

}
