using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "ScriptableObject/CollectibleInfo")]


public class CollectibleData : ScriptableObject
{

    [SerializeField] public  string[] CollectiblesName; 
    
}
