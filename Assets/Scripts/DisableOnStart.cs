using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnStart : MonoBehaviour
{
    [SerializeField] private float _DisableTimer = 1f;

    Animator _animator;
    private void Awake()
    {
        _animator = GetComponent<Animator>();
        if (_animator != null)
        {
            float time = 0; 
            AnimatorStateInfo _animStateInfo = _animator.GetCurrentAnimatorStateInfo(1);
            AnimationClip[] clips = _animator.runtimeAnimatorController.animationClips;
            foreach( AnimationClip thisClip in clips)
            {
                time += thisClip.length;
            }
            Debug.Log( "CLIP TIME : " +time);
            _DisableTimer = time;
        }
    }

    private void OnEnable()
    {
        Invoke("DisableCam", _DisableTimer);
    }

    private void DisableCam()
    {
        this.gameObject.SetActive(false);
    }

}
