using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ZoomOutZone : MonoBehaviour
{
    [SerializeField] private GameObject _thisCam;

    private void Awake()
    {
        _thisCam.SetActive(false);
    }

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag =="Player")
        _thisCam.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
            _thisCam.SetActive(false);        
    }
}
