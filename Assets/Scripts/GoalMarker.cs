using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalMarker : MonoBehaviour
{
    
    [SerializeField] GameObject _targetToGoTo;
    [SerializeField] float _rotationSpeed = 10f;

    [SerializeField] GameObject _arrowRender;
    [SerializeField] GameObject _nearRender;

    [Header("Platforms and objectives ")]
    [SerializeField] private GameObject _landingPlatform, _objectivePlatform;
    [SerializeField] private List<GameObject> _pickupObjList;
    


    public enum FollowType
    {
        Rigid,
        Lerp,
        Slerp,
    }

    //Populate in Inspector with you players transform
    public Transform _playerFollow;

    //The distance the camera will be from the player
    public Vector3 FollowOffset = new Vector3(0, 0, -10);

    //How quickly the camera moves
    public float FollowSpeed = 5f;

    //How the camera will follow
    public FollowType FollowMethod = FollowType.Lerp;

    //private Transform transform;
    private Vector3 _targetPlayer;

    //to check the distance between the player and the Target position
    private Vector3 _distanceV;
    private float _distanceF;

    //The Near Distance between the player and the landing platfrom when the thiing should dissapear 
    public float _nearDistance = 30f ;

    


    private void Start()
    {

        if(_playerFollow == null)
            _playerFollow = GameObject.FindGameObjectWithTag("Player").transform;
        if(_landingPlatform == null)
            _landingPlatform = GameObject.FindGameObjectWithTag("LevelFinishPlat");
        if (_objectivePlatform == null)
            _objectivePlatform = GameObject.FindGameObjectWithTag("ObjectivePlat");

        _targetToGoTo = _landingPlatform;


        SetBoxTarget();
    }

    public void AddOBJtoList(GameObject obj)
    {
        _pickupObjList.Add(obj);
        SetBoxTarget();
    }

    public void RemoveOBJtoList(GameObject obj)
    {
        _pickupObjList.Remove(obj);
        SetBoxTarget();


    }

    public void SetObjectivePlatfromTarget()
    {
        _targetToGoTo = _objectivePlatform;
    }

    public void SetBoxTarget()
    {
        //FINDS THE CLOSEST BOX IN THE LIST AND IT WILL SET THAT AS TARGET 
        float distance = 10000;
        Vector3 distanceV3; 

        if(!AreAllObjectsInactive())
        {
            foreach (GameObject obj in _pickupObjList)
            {
                distanceV3 = obj.transform.position - _targetPlayer;
                if (distance > distanceV3.magnitude )
                {
                    distance = distanceV3.magnitude;
                    _targetToGoTo = obj;
                }
            }
        }
        else
            _targetToGoTo = _landingPlatform.gameObject;


    }

    bool AreAllObjectsInactive()
    {
        foreach (GameObject obj in _pickupObjList)
        {
            if (obj.activeSelf)
            {
                return false; // If any object is active, return false
            }
        }
        return true; // If all objects are inactive, return true
    }



    void LateUpdate()
    {
        if(_targetToGoTo != _landingPlatform.gameObject)
        {
            if (AreAllObjectsInactive())
            {
                _targetToGoTo = _landingPlatform.gameObject;
            }
        }
        
        
        
        // ROTATE THE CAMERA ON ONE ANGLE TO POINT TOWARDS THE PLATFORM 
        if (_targetToGoTo != null)
        {
            Vector3 direction = _targetToGoTo.transform.position - base.transform.position;

            // Calculate the angle between the object's current direction and the target
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            // Smoothly rotate towards the target angle
            Quaternion targetRotation = Quaternion.Euler(new Vector3(0, 0, angle));
            base.transform.rotation = Quaternion.Slerp(base.transform.rotation, targetRotation, _rotationSpeed * Time.deltaTime);
        }



        // FOLLOW THE PLAYER 
        switch (FollowMethod)
        {
            case FollowType.Rigid:
                _targetPlayer = _playerFollow.position + FollowOffset;
                break;
            case FollowType.Lerp:
                _targetPlayer = Vector3.Lerp(transform.position, _playerFollow.position + FollowOffset, Time.deltaTime * FollowSpeed);
                break;
            case FollowType.Slerp:
                _targetPlayer = Vector3.Slerp(transform.position, _playerFollow.position + FollowOffset, Time.deltaTime * FollowSpeed);
                break;
        }

        transform.position = _targetPlayer;



        //CODE FOR WHEN PLAYER IS NEAR THE OBJECTIVE 
        _distanceV = transform.position - _targetToGoTo.transform.position;
        _distanceF = _distanceV.magnitude;

        if(_distanceF <= _nearDistance)
        {
            DeactivateArrow();
        }
        else
        {
            ReactivateArrow();
        }

        
    }





    private void DeactivateArrow()
    {
        if (_arrowRender != null)
            _arrowRender.SetActive(false);

    }

    private void ReactivateArrow()
    {
        if (_arrowRender != null)
            _arrowRender.SetActive(true);

    }

}
