using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Rendering;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public SoundE[] musicSound;
    public SoundE[] sfxSound;
    public AudioSource musicSource, sfxSource;

    public float _cheerVolumeAdjust;

    private float _defaultMusicVolume;

    private bool _isPlayingRandomMusic = false;  

    int musicIndex = 0;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else 
        {
            Destroy(gameObject);
        }
        _defaultMusicVolume = musicSource.volume;

    }

    private void Start()
    {
        //PlayMusic("Theme");
        //PlayRandomMusic();
    }


    public void PlayRandomMusic()
    {
        // Used to select Random Music 
        
        int i = musicSound.Length;
        i = UnityEngine.Random.Range(0, i);
        string sound = musicSound[i]._name;
        Debug.Log("Playing Music"+ sound);
        PlayMusic(sound);
    }

    

    public void PlayMusic(string name)
    {
        SoundE s = Array.Find(musicSound, x => x._name == name);
        float musicLenght;
        if(s ==null)
        {
            Debug.Log("Sound Not Found ");
        }
        else
        {
            if (s._name == "Cheering")
            {
                musicSource.volume = _cheerVolumeAdjust;
            }
            else
            {
                musicSource.volume = _defaultMusicVolume;
            }
            musicSource.clip = s.clip;
            musicLenght = s.clip.length;
            musicSource.Play();

            if(PlayerPrefs.GetInt("RandomMusicToggle")==1)
            {
                Invoke("PlayRandomMusic", musicLenght*4);
            }

        }
        
    }

    public void CancelRandomMusic()
    {
        CancelInvoke("PlayRandomMusic");

    }

    public void PlaySFX(string name)
    {
        SoundE s = Array.Find(sfxSound, x => x._name == name);

        if (s == null)
        {
            Debug.Log("Sound Not Found ");
        }
        else
        {
            sfxSource.clip = s.clip;
            sfxSource.PlayOneShot(s.clip);

        }
    }

    public void ToggleMusic()
    {
        musicSource.mute = !musicSource.mute;

    }

    public void ToggleSFX()
    {
        sfxSource.mute = !sfxSource.mute;
    }


    public void MusicVolume(float volume)
    {
        musicSource.volume = volume;

    }

    public void SFXVolume(float volume)
    {
        sfxSource.volume = volume; 
    }


}
