using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TractorBeam : RocketAttatch
{

    Rigidbody _LiftableObject;
    bool _isPull = false;
    bool _isAttatch = false;

    [Header("Parameters")]
    [SerializeField] float _pullForce = 100f;

    [Header("PArticle System")]
    [SerializeField] ParticleSystem _psTractor;
    [SerializeField] ParticleSystem _psTractorFail;

    private LiftableObject _LOScript;

    float distanceBtwn;
    FixedJoint _fj;
    FixedJoint _fj_delete; // This is the existing joint a moving platform might have which is supposed to be broken
    
    public override void Action()
    {
        base.Action();
        //PS_vfxStart();
        
        if (!_isAttatch) // IF nothing is attatched the object will pull 
        {

            
            if(_isPull )
            {
                _isPull = false;
                PS_vfxStop(); 
            }
            else
            {
                if (_LiftableObject != null)
                {
                    _isPull = true;
                    _psTractor.Play();

                }
                else
                {
                    _psTractorFail.Play();
                }
            }
            



        }
        else
        {
            // CODE FOR IF SOMETHING IS ATTATCHED 
            AttachToPlayer(); // THIS FUNCTION ALSO DETATCHS FROM PLAYER 
        }
        

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "LiftableObject")
        {
            //Debug.Log("Tractor Beam Working ");
            _LiftableObject = other.gameObject.GetComponent<Rigidbody>();
            _LOScript = _LiftableObject.GetComponent<LiftableObject>();
            if(_LOScript != null ) 
                _LOScript.ActivateGlow();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "LiftableObject")
        {
            if (_LOScript != null)
            {
                _LOScript.DeactivateGlow();
            }
            _LiftableObject = null;

            _isPull = false;
            PS_vfxStop();
        }
    }
  

    private void FixedUpdate()
    {
        base.Update();
        if (_isPull && !_isAttatch)
        {
            Pull();

        }
    }

    private void Pull()
    {
        if (_LiftableObject != null)
        {
            _fj_delete = _LiftableObject.gameObject.GetComponent<FixedJoint>();
            if (_LiftableObject.gameObject.GetComponent<FixedJoint>() != null)
            {
                _fj_delete.breakForce = 0;
                Debug.Log(" CODE TO BREAK THE TIES "); 
            }


            Vector3 directionToPlayer = transform.position - _LiftableObject.position;
            _LiftableObject.AddForce(directionToPlayer.normalized * _pullForce);
            distanceBtwn = directionToPlayer.magnitude;
            //Debug.Log("Magnitude "+ distanceBtwn);

            if (distanceBtwn <= 0.2 )
            {
                
                _isPull = false;
                AttachToPlayer();
                PS_vfxStop();
            }
        }

    }

    private void AttachToPlayer() // CAN ALSO DETATCH FROM PLAYER 
    {
        if (!_isAttatch)
        {
            if (_LiftableObject != null)
            {
                _isAttatch = true;
                _fj = _LiftableObject.gameObject.GetComponent<FixedJoint>();
                
                if(_fj == null)
                    _fj  = _LiftableObject.gameObject.AddComponent<FixedJoint>(); // Will add the Fixed joint if it does not exist; 
                //_fj = _LiftableObject.GetComponent<FixedJoint>();

                if (_fj != null)
                {
                    _fj.connectedBody = this.gameObject.transform.parent.GetComponent<Rigidbody>();
                    
                    // Attatch the thing to the player
                }

            }

        }
        else
        {
            // DETATCH 
            if (_fj != null)
            {
                Destroy(_fj);

            }
            _isAttatch = false;
            PS_vfxStop();
        }
        
       
    }

    private void PS_vfxStart()
    {
        _psTractor.Play();
    }

    private void PS_vfxStop()
    {
        _psTractor.Stop();
    }
}