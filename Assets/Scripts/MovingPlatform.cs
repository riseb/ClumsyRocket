using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.UIElements;
using UnityEditor;

using System.Diagnostics.CodeAnalysis;
using TMPro;
using Unity.VisualScripting;

public class MovingPlatform : MonoBehaviour
{
    [Tooltip("Keep Point A at LEFT SIDE ")]
    [SerializeField] private Transform _pointA, _pointB;
    private Transform _targetPosition;
    [SerializeField] private float _duration =1f;
    private bool _toPointA = true;

    [SerializeField] private float _pullForce = 10f;
    [SerializeField] private float _pullBoxForce = 20f;

    private FixedJoint _fj;
    [SerializeField] private float _pullDistance = 10f;
    
    private bool _isPull;
    private GameObject _pullObject;
    Vector3 _direction;
    Rigidbody _rb, _thisRB;

    private DG.Tweening.Sequence movementSequence;
    int _loop = -1;
     // Start is called before the first frame update
    void Start()
    {
        //MovebetweenPoints();
        _toPointA = true;
        _targetPosition = _pointA;
        _isPull = false;
        _thisRB = GetComponent<Rigidbody>();

        MovebetweenPoints();
    }

    private void MovebetweenPoints()
    {
        movementSequence = DOTween.Sequence();

        // Add movement to the sequence
        movementSequence.Append(transform.DOMove(_pointB.position, _duration).SetEase(Ease.Linear))
                        .Append(transform.DOMove(_pointA.position, _duration).SetEase(Ease.Linear))
                        .SetLoops(-1, LoopType.Yoyo); // Repeat indefinitely
        

    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag =="LiftableObject")
        {
            _pullObject = other.gameObject;
            _rb = other.gameObject.GetComponent<Rigidbody>();

            Debug.Log(_pullObject);

            _fj = _pullObject.gameObject.GetComponent<FixedJoint>(); // Checks if the FJ is already connected to player 
            
            if (_fj == null)
                _fj = _pullObject.gameObject.AddComponent<FixedJoint>(); // Adds an FJ if it is not connected to player
            
            if(_fj.connectedBody == null )
                _fj.connectedBody = this.gameObject.GetComponent<Rigidbody>(); // sets parent as FJ unless the FJ is already connected to the body 
            
     
            
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "LiftableObject")
        {
            
            other.gameObject.transform.SetParent(null);
            
            if (_toPointA)
                _direction = Vector3.left;
            else
                _direction = Vector3.right;


            Vector3 dir = Vector3.up + _direction;
            //_rb.AddForce(dir * _pullForce, ForceMode.Impulse);

            

        }

    }
    


    private void FixedUpdate()
     {

       

        if (_rb != null && _fj ==null)
            PullBoxIfClose();
     }

    void PullBoxIfClose()
    {
        

        Vector3 distanceToPlayer = transform.position - _pullObject.transform.position;
        if (distanceToPlayer.magnitude < _pullDistance)
        {
            _rb.AddForce(distanceToPlayer.normalized * _pullBoxForce);
        }
        else _rb = null;

        Debug.Log("Distance :"+ distanceToPlayer.magnitude);
    }
    
}
