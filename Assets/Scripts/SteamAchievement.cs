#if !(UNITY_STANDALONE_WIN || UNITY_STANDALONE_LINUX || UNITY_STANDALONE_OSX || STEAMWORKS_WIN || STEAMWORKS_LIN_OSX)
#define DISABLESTEAMWORKS
#endif

#if !DISABLESTEAMWORKS
using Steamworks;
#endif

using UnityEngine;



public class SteamAchievement : MonoBehaviour
{
    #if !DISABLESTEAMWORKS
    // Start is called before the first frame update

    // Basically use this function for all the achievement related code 
   public void LevelCompleteAchievement(string achievmentSTR)
    {
        // Call this Function when a Level is Completed 

        if(SteamManager.Initialized)
        {

            SteamUserStats.GetAchievement(achievmentSTR, out bool bAchievementComplete);

            if (!bAchievementComplete)
            {
                SteamUserStats.SetAchievement(achievmentSTR);
                Debug.Log("Steam Achievement Code Called");
                SteamUserStats.StoreStats();
            }

        }
    }



    public void LavaisBadAchievement()
    {
        if (SteamManager.Initialized)
        {
            string achievmentSTR = "ACH_LAVA_BAD";
            LevelCompleteAchievement(achievmentSTR);

        }
    }


    public void OnDeath()
    {
        // THIS FUNCTION COUNTS THE NUMBER OF DEATHS AND GIVES ACHIEVEMENTS BASED ON THAT // 
        // LET THE PLAYERS DIE ! 

        
       
        if (SteamManager.Initialized)
        {
            // This code will set the FIRST DEATH of the Player// 
            string achievmentSTR = "ACH_FIRST_DEATH";
            LevelCompleteAchievement(achievmentSTR);
            // Now since the first death is done, lets focus on the counting the deaths 

            SteamUserStats.GetStat("DEATH_COUNT", out int deathCountSteam);
            deathCountSteam++;
            SteamUserStats.SetStat("DEATH_COUNT",deathCountSteam); // this will set the deaths in steam // 
            SteamUserStats.StoreStats();



            if(deathCountSteam >= 10)
                LevelCompleteAchievement("ACH_DEATH_10");

            if (deathCountSteam >= 25)
                LevelCompleteAchievement("ACH_DEATH_25");

            if (deathCountSteam >= 50)
                LevelCompleteAchievement("ACH_DEATH_50");

            if (deathCountSteam >= 100)
                LevelCompleteAchievement("ACH_DEATH_100");

            if (deathCountSteam >= 69)
                LevelCompleteAchievement("ACH_DEATH_69");


            Debug.Log("DEATH COUNT :"+ deathCountSteam );
        }
    }

    public void OnDataDicksCollected()
    {
        if(SteamManager.Initialized)
        {
            SteamUserStats.GetStat("DATA_DISK_COUNT", out int DataDicksCount);
            Debug.Log("EXISTING DATA DICKS : "+ DataDicksCount);
            DataDicksCount++;
            SteamUserStats.SetStat("DEATH_COUNT", DataDicksCount); //this will set the deaths in steam
            SteamUserStats.StoreStats();
        }
    }
#endif
}
