using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceGlyphScript : MonoBehaviour
{

    [SerializeField] GameObject[] _glyphKeyboard, _glyphXbox, _GlyphPs5;

    private void Update()
    {
        // GOTTA FIND A BETTER WAY OF REPLACING THIS UPDATE SCRIPT 
        //ChangeGlyph();
    }

    private void OnEnable()
    {
        DeviceDetectScript.onControllerChange += ChangeGlyph;
    }

    private void OnDisable()
    {
        DeviceDetectScript.onControllerChange -= ChangeGlyph;
    }

    private void Start()
    {
        ChangeGlyph();
    }

    public void ChangeGlyph()
    {
        //Debug.Log("CHANGING CONTROLLLERRR");

        switch (GameManager._device)
        {
             case 0: // Keyboard
                foreach( GameObject gm in _glyphKeyboard ) gm.SetActive(true);
                foreach (GameObject gm in _glyphXbox) gm.SetActive(false);
                foreach (GameObject gm in _GlyphPs5) gm.SetActive(false);

                
                break;
             case 1: // XBOX
                foreach (GameObject gm in _glyphKeyboard) gm.SetActive(false);
                foreach (GameObject gm in _glyphXbox) gm.SetActive(true);
                foreach (GameObject gm in _GlyphPs5) gm.SetActive(false);

                break;
             case 2: // PS5 
                foreach (GameObject gm in _glyphKeyboard) gm.SetActive(false);
                foreach (GameObject gm in _glyphXbox) gm.SetActive(false);
                foreach (GameObject gm in _GlyphPs5) gm.SetActive(true);
                break;

            default:
                
                break;
        }

    }
}
