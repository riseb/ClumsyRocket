
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class AnitPlayerTurret : MonoBehaviour
{
    public UnityEvent onEventTriggered;

    [SerializeField] Transform SpawnPosition;
    [SerializeField] GameObject _bulletEnemy;
    [SerializeField] float _timeTillRespawn;

    private bool _isActive;
    private bool _isSpawning;
    private bool _isAlive ;

    private void Start()
    {
        _isAlive = true;
        _isActive = false;
        _isSpawning = true;
        _bulletEnemy.SetActive(false);
        _bulletEnemy.transform.position = SpawnPosition.transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && !_isActive &&_isAlive)
        {
            
            _isActive = true;
            _bulletEnemy.SetActive(true);
        }
    }

    public void StopRespawningBullets()
    {
        Debug.Log("No More Bullet");
        _isSpawning = false ;
        _isAlive = false;

        if (onEventTriggered != null)
        {
            onEventTriggered.Invoke();
        }
           
    }


    public void RespawnBulletAfterTime()
    {
        if(_isSpawning && _isAlive)
        {
            

            Invoke("RespawnBullet", _timeTillRespawn);
        }
    }

    private void RespawnBullet()
    {
        if (_isSpawning)
        {
            Debug.Log("Bullet REspawned");
            _bulletEnemy.transform.position = SpawnPosition.transform.position;
            _bulletEnemy.SetActive(true);
        }
            
    }



}
