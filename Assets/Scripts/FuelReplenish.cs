using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelReplenish : MonoBehaviour
{
    bool _refuelPlayer = false;
    PlayerController _player;
    
    void Update()
    {
        if (_refuelPlayer && _player != null)
        {
            _player.FuelReplete();
        }

    }

    

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.tag == "Player")
        {


            _refuelPlayer = true;
            _player = other.GetComponent<PlayerController>();
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            _refuelPlayer = false;
            _player = null;

        }
    }
}
