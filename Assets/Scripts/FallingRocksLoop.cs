using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening; 

public class FallingRocksLoop : MonoBehaviour
{
    // Start is called before the first frame update

    [Header("---------ROCKS------------")]
    [SerializeField] private GameObject _fallingRocks ;
    [SerializeField] private float _duration; // Duration of the Rock Falling 
    [SerializeField] private float _delay;  // Duration after which the Rock will Start Falling 
    [SerializeField] private Transform _endLocation;
    private Transform _startLoc;
     




    private void Awake()
    {
        
        _startLoc = this.transform;


        StartCoroutine(MoveRockDelay(_delay));

    }


    IEnumerator MoveRockDelay(float delayTime)
    {
        // Wait for the specified delay time
        yield return new WaitForSeconds(delayTime);

        MoveRock();
    }




    private void MoveRock()
    {
        // Create a sequence
        Sequence sequence = DOTween.Sequence();

        // Add a move tween to the sequence
        sequence.Append(_fallingRocks.transform.DOMove(_endLocation.transform.position, _duration).SetEase(Ease.Linear));


        // Set the loop count and type
        sequence.SetLoops(-1, LoopType.Restart);
    }


   

}
