using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCollect : MonoBehaviour
{

    private bool _isStillOnBox;
    [SerializeField] private float _eatTime = 5f;
    [SerializeField] private string _pickupOBJtag;
    [SerializeField] LevelManager _levelManager;
    private GameObject _pickupItem;
    private LiftableObject _liftableObject;
    [SerializeField] ParticleSystem _PS_teleport;

    private void Awake()
    {
        _isStillOnBox = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == _pickupOBJtag)
        {
            _isStillOnBox = true;
            Invoke("EatBox",_eatTime);
            _pickupItem = other.gameObject;
            _liftableObject = _pickupItem.GetComponent<LiftableObject>();
            _PS_teleport.transform.position = other.transform.position - 2*Vector3.up;

            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == _pickupOBJtag)
        {
            _isStillOnBox = false;
            

        }
    }

    private void EatBox()
    {
        // IF the box is still in collision then eat the box 
        if(_isStillOnBox)
        {
            if (_pickupItem != null)
            {
                _PS_teleport.Play();
                _PS_teleport.transform.position = _pickupItem.transform.position - 2 * Vector3.up;
                //Destroy(_pickupItem,_PS_teleport.time);
                if(_liftableObject != null)
                    _liftableObject.DestroyNow(); 
                

                if (_levelManager != null)
                {
                    _levelManager.PickupCollected();
                }
                else
                {
                    Debug.LogError("REFERENCE MISSING DUMBASS");
                }
            }
        }
    }


    
}
