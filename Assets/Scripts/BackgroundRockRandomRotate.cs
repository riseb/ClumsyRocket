using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundRockRandomRotate : MonoBehaviour
{
    

    public float rotationDuration = 100f; // Time to complete each rotation loop
    public float rotationSpeed = 50f;   // Speed of random rotation
    private Vector3 randomRotationAxis;
    private Vector3 randomAxis; // The random axis to rotate around

    void Start()
    {
        // Choose a random axis to rotate around (X, Y, or Z)
        randomAxis = GetRandomAxis();
    }

    void Update()
    {
        // Rotate the object around the chosen axis at a constant speed
        transform.Rotate(randomAxis * rotationSpeed *2f * Time.deltaTime);
    }

    // Method to get a random axis (X, Y, or Z)
    Vector3 GetRandomAxis()
    {
        // Randomly choose between X, Y, or Z for the axis of rotation
        int axisChoice = Random.Range(0, 3);
        switch (axisChoice)
        {
            case 0: return Vector3.right;  // Rotate around the X axis
            case 1: return Vector3.up;     // Rotate around the Y axis
            case 2: return Vector3.forward; // Rotate around the Z axis
            default: return Vector3.up;    // Default to rotating around Y
        }
    }
}
