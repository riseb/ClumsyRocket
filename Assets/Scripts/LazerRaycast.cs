
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerRaycast : MonoBehaviour
{

    Ray _ray;
    RaycastHit _hit;

    

    [SerializeField] GameObject _lazerMesh;
    
    ParticleSystem.MainModule _psMain;
    private void Start()
    {
        _ray = new Ray(transform.position, transform.forward);

    }
    

    void Update()
    {
        Vector3 rayDirection = transform.TransformDirection(Vector3.forward);

        if (Physics.Raycast(transform.position, -transform.up, out _hit))
        {
            //Debug.Log(_hit.transform.gameObject.tag);
            
            _lazerMesh.transform.localScale = new Vector3(0.4f,_hit.distance,0.4f);

            if(_hit.transform.gameObject.tag == "Player")
            {
                PlayerController _thisPlayer = _hit.transform.gameObject.GetComponent<PlayerController>();

                _thisPlayer.Death();
            }
        }
    }
            
}
