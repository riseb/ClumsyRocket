using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class MagneticPull : MonoBehaviour
{

    [SerializeField] private float _pullForce = 40f; // the strenght with which it will pull player 
    [SerializeField] private Transform _target;
    // PRIVATE VARIABLES
    private GameObject _player;
    private Rigidbody _playerRb;

    private Vector3 _directionToPlayer;

    private Vector3 _pullDirection;

    [SerializeField] ParticleSystem _psMagBeam;
    [SerializeField] float _PSsimSpeed = 1f;

    public UnityEvent GravityAchievement;
    [SerializeField] float _gravityAchievementTimer = 20f;
    private bool _isStillInside;
    private void Start()
    {
        _isStillInside = false;

        if (_psMagBeam == null)
        {
            _psMagBeam = gameObject.GetComponentInChildren<ParticleSystem>();

        }

        if(_psMagBeam!=null)
        {
            var main = _psMagBeam.main;
            main.simulationSpeed = _PSsimSpeed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Start Pulling the Player
        if(other.tag == "Player")
        {
            Debug.Log("Player Dicktected");
            _player = other.gameObject;
            _playerRb = other.GetComponent<Rigidbody>();
            _isStillInside = true;
            Invoke("ActivateGravityAchievement", _gravityAchievementTimer);
        }    
    }

    private void OnTriggerExit(Collider other)
    {
        // Stop Pulling the Player
        if (other.tag == "Player")
        {
            _player = null;

            _playerRb =null;
            _isStillInside = false;
        }

        
    }

   

    private void FixedUpdate()
    {
        if (_playerRb != null)
        {
           Pull();
        }
    }

    private void Pull()
    {
        //Debug.Log("Pulling the Object");
        _pullDirection = _target.position - transform.position;
        _directionToPlayer = _target.position - _player.transform.position;
        _playerRb.AddForce(_pullDirection.normalized * _pullForce);
    }
    

    void ActivateGravityAchievement()
    {
        if(_isStillInside)
        {
            GravityAchievement?.Invoke();
            Debug.Log("GravityAchievement");
        }
    }

    
}
