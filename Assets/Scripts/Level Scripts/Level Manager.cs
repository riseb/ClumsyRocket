using DialogueEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private ConversationTrigger _startConversation;

    [SerializeField]
    private bool _startConvoDone;

    [SerializeField] protected float _pointsRequired;

    private float _taskPoints;

    [SerializeField]protected NextLevel _nextLevelScript;

    [SerializeField] private Camera _mainCamera;
    [SerializeField] private Animator _animator;

    [SerializeField] private LevelList _lvlList;

    //Collectibles
    private GameObject[] _collectibles;
    
    private int _collectiblesNum;

    public virtual void Start()
    {

        GameManager.LevelStart();
        _taskPoints = 0f;
        ConversationIntro();

        _collectibles = GameObject.FindGameObjectsWithTag("Collectible");
        _collectiblesNum = _collectibles.Length; 
        if (_collectibles != null)
        {
            foreach (GameObject obj in _collectibles)
            {

                Collectible c = obj.GetComponent<Collectible>();
                //c.SetMananger(this);
            }
        }
           
    }


    private void ConversationIntro()
    {

        GameObject gm = GameObject.FindGameObjectWithTag("IntroScript");
        if (gm != null)
            _startConversation = gm.GetComponent<ConversationTrigger>();

        if (_startConversation != null)
        {
            
            if (GameManager._Instance._newLevelStarted)
            {

                _startConversation.StartConversation();
                ConversationManager.Instance.SetBool("IntroDone", GameManager._Instance._newLevelStarted);
                Debug.Log(ConversationManager.Instance.GetBool("IntroDone"));
            }
           
        }
    }


    public virtual void PickupCollected()
    {
        //Debug.Log("Adding Points ");

        _taskPoints++;
        if (_taskPoints >= _pointsRequired)
        {
            _nextLevelScript.ObjectiveComplete();
        }
    }

    public virtual void CollectibleCollected()
    {
        _nextLevelScript.CollectibleScore(_collectiblesNum);

    }
}
