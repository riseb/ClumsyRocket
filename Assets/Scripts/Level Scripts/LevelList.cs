using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/LevelList")]


public class LevelList : ScriptableObject
{
    
    [SerializeField] public string[] LevelName;

    [SerializeField] public string[] LevelDisplayNameForPublic;

    [SerializeField] public LevelCompleteData[] LevelCompleteData;
}
