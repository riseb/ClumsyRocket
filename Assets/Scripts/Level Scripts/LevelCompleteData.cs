using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;


[CreateAssetMenu(menuName = "ScriptableObject/LevelInfo")]
public class LevelCompleteData : ScriptableObject
{
    [SerializeField] public string _levelName;
    [SerializeField] public float _TimeStar0; 
    [SerializeField] public float _TimeStar1;
    [SerializeField] public float _TimeStar2;
    [SerializeField] public float _TimeStar3;
    [SerializeField] public float _TimeStar4;
    [SerializeField] public string _dataBaseName;
    [SerializeField] public Sprite _image;
    public int StarsDisplay(float time)
    {
        int stars = 4;
        if (time > _TimeStar4) stars--;
        if (time > _TimeStar3) stars--; 
        if (time > _TimeStar2) stars--;
        if (time > _TimeStar1) stars--;
        

        //Debug.Log("STARS : " + stars);
        return stars;
    }

    public float starTiming(int i)
    {
        switch(i)
        {
            case 4:
                return _TimeStar4;
            case 3:
                return _TimeStar3;
            case 2:
                return _TimeStar2;   
            default:
                return 0f;
        }
    }
}
