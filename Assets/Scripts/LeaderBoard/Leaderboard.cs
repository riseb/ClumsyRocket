using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Dan.Main;
public class Leaderboard : MonoBehaviour
{

    [SerializeField] private List<TextMeshProUGUI> names , scores;
    [SerializeField] private LevelCompleteData _lvldata; 

    private string publicLeaderboardKey = "81e67b7fa0dd1198ceafc13d5cf05cdd12602d900cc9d14369cdef49ba19657a";

    private void Awake()
    {
        //publicLeaderboardKey = _lvldata.publicKey;
        GetLeaderboard();
    }
    public void GetLeaderboard()
    {
        if (publicLeaderboardKey != null)
        {
            Debug.Log("Getting LeaderBoard");

            LeaderboardCreator.GetLeaderboard(publicLeaderboardKey, ((msg) =>
            {
                int loopLenght = (msg.Length < names.Count) ? msg.Length : names.Count;
                for (int i = 0; i < loopLenght; i++)
                {
                    names[i].text = msg[i].Username;
                    scores[i].text = msg[i].Score.ToString();
                }
            }));
        }
        
    }

    public void SetLeaderboardEntry(string username, int score)
    {
        if (publicLeaderboardKey != null)
        {
            LeaderboardCreator.UploadNewEntry(publicLeaderboardKey, username, score,
                        ( (msg) =>{ GetLeaderboard(); } ) ) ;
        }
            

        
    }
}
