using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SkinsMenu : MonoBehaviour
{

    // THIS SCRIPT IS FOR THE SKINS MENU 


    int _SkinIndex, _prevSkinIndex; // current skin indexx // previouys skin index 
    [SerializeField] private int _MaxSkins; // maximum skins unlocked 
    [SerializeField] CollectibleData _CollectibleData;
    [SerializeField] GameObject _unknownSkin;
    [SerializeField] GameObject[] _skinsImageUI;
    [SerializeField] TextMeshProUGUI _TMPSkinindicator;
    [SerializeField] TextMeshProUGUI _TMPSkinindicator2;
    /// <summary>
    ///  PlayerPref "SkinIndex" is set for the player to be able to use that skin 
    ///  PlayerPref "_unlockSKin" tells the system if the skin is unlocked or locked ( required data disk is collected ) 
    /// </summary>

    private void Awake()
    {

        string SkinName = "Skin";
        string tempSkinName;


        if (PlayerPrefs.HasKey("SkinIndex"))
        {
            _SkinIndex = PlayerPrefs.GetInt("SkinIndex");
        }
        else
        {
            _SkinIndex = 0;
            PlayerPrefs.SetInt("SkinIndex",0);
        }
            

        InitialiseCollectibles();
        _prevSkinIndex = _SkinIndex;
        SetSkinImageUI();
        _unknownSkin.SetActive(false);

        
    }


    private void InitialiseCollectibles()
    {
        //This function will initialise the collectibles PlayerPrefs if they do not already exist 
        int length = _CollectibleData.CollectiblesName.Length;

        for (int i = 0; i < length; i++)
        {
            _skinsImageUI[i].SetActive(false);

            if(!PlayerPrefs.HasKey(_CollectibleData.CollectiblesName[i]))
            {
                Debug.Log("Does not contain" + i);
                PlayerPrefs.SetInt(_CollectibleData.CollectiblesName[i],0);
            }
        }

        
    }

    public void NextButton()
    {
        if(_SkinIndex+1 < _MaxSkins)
        {
            _prevSkinIndex = _SkinIndex;
            _SkinIndex++;
            
            SetSkin();
            SetSkinImageUI();
        }
          
        Debug.Log("Skin Index : "+ _SkinIndex);
    }

    public void PrevButton()
    {
        if (_SkinIndex - 1 > - 1 )
        {
            _prevSkinIndex = _SkinIndex;
            _SkinIndex--;
            
            SetSkin();
            SetSkinImageUI();
        }
            
        Debug.Log("Skin Index : " + _SkinIndex);
    }

    public void SetSkin()
    {

        if (PlayerPrefs.GetInt("Skin" + _SkinIndex) == 1)
        {
            _unknownSkin.SetActive(false);
            Debug.Log("SkinIndex :" +_SkinIndex + "Equipped");
            PlayerPrefs.SetInt("SkinIndex", _SkinIndex);
        }
        else
            _unknownSkin.SetActive(true);
    }

    public void SetSkinImageUI()
    {

        _skinsImageUI[_prevSkinIndex].SetActive(false);
        
        _skinsImageUI[_SkinIndex].SetActive(true);

        _TMPSkinindicator.text = (_SkinIndex+1).ToString() + " / " + _MaxSkins.ToString();
        _TMPSkinindicator2.text = (_SkinIndex + 1).ToString() + " / " + _MaxSkins.ToString();
    }
    
}
