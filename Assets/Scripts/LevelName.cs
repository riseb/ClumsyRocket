using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelName : MonoBehaviour
{
    string lvlName;

    [SerializeField] public TextMeshPro _LevelTextTMP;
    
    void Awake()
    {
        lvlName = SceneManager.GetActiveScene().name;
        lvlName = "Level: " + lvlName;
        _LevelTextTMP.text = lvlName;

    }
}
