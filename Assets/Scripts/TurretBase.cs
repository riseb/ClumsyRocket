using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class TurretBase : MonoBehaviour
{
    public UnityEvent TurretDestroyEvent;


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag =="Bullet")
        {
            TurretDestroyEvent?.Invoke();

            Bullet _thisBullet = other.GetComponent<Bullet>();

            _thisBullet.explode();


            this.gameObject.SetActive(false);   
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            TurretDestroyEvent?.Invoke();
        }
    }

}
