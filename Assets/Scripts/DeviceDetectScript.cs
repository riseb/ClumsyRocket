using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class DeviceDetectScript : MonoBehaviour
{

    private string lastUsedDevice = "None";

    private bool _bDeviceChange = false;

    private List<DeviceGlyphScript>[] _glyphScriptsList;

    private bool _mouseClicked = false;

    private int _currentDeviceID; // 0 - Keyboard 1 - XBox , 2 - PS5

    public delegate void DeviceChangeEvent();
    public static event DeviceChangeEvent onControllerChange;

    
    public void detectGlyphScripts()
    {
        Debug.Log("DETECTING THE GLYPHS");
        _currentDeviceID = 0;
        
    }

    
    
    // Update is called once per frame
    void Update()
    {
        if (Keyboard.current.anyKey.wasPressedThisFrame)
        {
            lastUsedDevice = "Keyboard";
            //Debug.Log("Last used device: " + lastUsedDevice);
            _bDeviceChange = true;
            GameManager._device = 0;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            
        }

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))  // 0 = Left Click, 1 = Right Click
        {
            _mouseClicked = true;
            _bDeviceChange = false;
            lastUsedDevice = "Mouse";
            //Debug.Log("Last used device: " + lastUsedDevice);
            GameManager._device = 0;

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

        }

        // Check if a specific button on the controller was pressed (like A, B, or joystick axes)
        if (Gamepad.current != null)
        {
            if (Gamepad.current.buttonSouth.wasPressedThisFrame || // A Button (Xbox)
                Gamepad.current.buttonEast.wasPressedThisFrame ||  // B Button (Xbox)
                Gamepad.current.leftStick.ReadValue().magnitude > 0.1f || // Left stick movement
                Gamepad.current.rightStick.ReadValue().magnitude > 0.1f || // Right stick movement
                Gamepad.current.dpad.up.wasPressedThisFrame ||  // D-Pad Up
                Gamepad.current.dpad.down.wasPressedThisFrame || // D-Pad Down
                Gamepad.current.dpad.left.wasPressedThisFrame || // D-Pad Left
                Gamepad.current.rightShoulder.wasPressedThisFrame || // Right Shoulder
                Gamepad.current.leftShoulder.wasPressedThisFrame || // Left Shoulder
                Gamepad.current.rightTrigger.wasPressedThisFrame|| // Right Trigger
                Gamepad.current.leftTrigger.wasPressedThisFrame|| // LeftTrigger
                Gamepad.current.dpad.right.wasPressedThisFrame) 
            {
                lastUsedDevice = "Controller";
                //Debug.Log("Last used device: " + lastUsedDevice);
                _bDeviceChange = true;

                Cursor.lockState = CursorLockMode.Locked;

                if (Gamepad.current.name == "DualSenseGamepadHID") 
                    GameManager._device = 2;
                else
                    GameManager._device = 1;

            }


            
        }
        
        if (_mouseClicked && _bDeviceChange)
        {
            // CODE FOR THE EVENT SYSTEM 
            GameObject button = GameObject.FindGameObjectWithTag("Button");
            Debug.Log("Device Change Button Press ");

            if (button == null)
                button = GameObject.FindGameObjectWithTag("BackButton");

            if (button != null)
                EventSystem.current.SetSelectedGameObject(button);

            _bDeviceChange= false;
            _mouseClicked= false;
        }

        if(_currentDeviceID != GameManager._device)
        {
            if(onControllerChange != null)
            {
                onControllerChange();
                _currentDeviceID = GameManager._device;
            }
            
        }

    }
}
