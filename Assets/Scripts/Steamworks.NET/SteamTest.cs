
#if !(UNITY_STANDALONE_WIN || UNITY_STANDALONE_LINUX || UNITY_STANDALONE_OSX || STEAMWORKS_WIN || STEAMWORKS_LIN_OSX)
#define DISABLESTEAMWORKS
#endif

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SocialPlatforms.Impl;
using Unity.VisualScripting;
using System;

#if !DISABLESTEAMWORKS
using Steamworks;
#endif

public class SteamTest : MonoBehaviour
{
#if !DISABLESTEAMWORKS

    // Start is called before the first frame update
    public InputField _inputfield;

    [SerializeField] private string s_leaderboardName = "testboard";
    [SerializeField] private int _score = 1000;

    private SteamLeaderboard_t s_currentLeaderboard;
    private bool s_initialized = false;
    private CallResult<LeaderboardFindResult_t> m_findResult = new CallResult<LeaderboardFindResult_t>();
    private CallResult<LeaderboardScoreUploaded_t> m_uploadResult = new CallResult<LeaderboardScoreUploaded_t>();
    private CallResult<LeaderboardScoresDownloaded_t> m_downloadResult = new CallResult<LeaderboardScoresDownloaded_t>();
    public struct LeaderboardData
    {
        public string username;
        public int rank;
        public int score;
    }
    List<LeaderboardData> LeaderboardDataset;



    void Start()
      {
        if(SteamManager.Initialized)
        {
            string name = SteamFriends.GetPersonaName();
            Debug.Log(name);
        }
    }


    public void OnButtonPress()
    {
        Debug.Log("Button Pressed");

        // ON BUTTON PRESS WHATEVER NAME IS STORED SHOULD GO IN THE STUPID TABLE 
        if (_inputfield.text != null)
        {
            string text = _inputfield.text;
            Debug.Log(" TEXT TO STORE : "+ text);


            // Text is working fine , now lets store the text in the stupid Table 
            if (SteamManager.Initialized)
            {
                SteamAPICall_t hSteamAPICall = SteamUserStats.FindLeaderboard(s_leaderboardName);
                m_findResult.Set(hSteamAPICall, func1);
            }
        }
        
    }


    public void OnButtonPressScoreShow()
    {
        if (SteamManager.Initialized)
        {
            SteamAPICall_t hsteamAPICall = SteamUserStats.DownloadLeaderboardEntries(s_currentLeaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal,1,10);

            m_downloadResult.Set(hsteamAPICall, ScoreDownloaded);
        }
    }

    public void ScoreDownloaded(LeaderboardScoresDownloaded_t pCallback, bool failure)
    {
        if (!failure)
        {
            Debug.Log(pCallback.m_cEntryCount);

            for(int i = 0; i<pCallback.m_cEntryCount; i++ )
            {
                LeaderboardEntry_t entry;
                SteamUserStats.GetDownloadedLeaderboardEntry(pCallback.m_hSteamLeaderboardEntries,i, out entry,null,0);

                Debug.Log($"User {SteamFriends.GetFriendPersonaName(entry.m_steamIDUser)} - {entry.m_nScore} - rank: {entry.m_nGlobalRank}");

                // HERE YOU CAN UPDATE THE UI LMAO // IM USING LMAO ALOT NOWADAYS 
            }

        }
        else
            Debug.Log("Failed to Retreive Leaderbaord Scores ");
    }



   public void func1(LeaderboardFindResult_t pCallback, bool failure)
    {
        Debug.Log($"Steam Leaderboard Find: Did it fail? {failure}, Found: {pCallback.m_bLeaderboardFound}, leaderboardID: {pCallback.m_hSteamLeaderboard.m_SteamLeaderboard} " );
        s_currentLeaderboard = pCallback.m_hSteamLeaderboard;
        s_initialized = true;
        UploadSCore(_score);
    }

    public void UploadSCore(int score)
    {
        SteamAPICall_t handle = SteamUserStats.UploadLeaderboardScore(s_currentLeaderboard,                     // The leaderboard handle
            ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodKeepBest, // How to handle score uploads (e.g., keep best, force update)
            score,                                 // The score to upload
            null,                                  // Optional: an array of details (e.g., extra data associated with the score)
            0                                      // Optional: the count of details if provided
        );
        m_uploadResult.Set(handle, ScoreUploaded);
    }

    public void ScoreUploaded(LeaderboardScoreUploaded_t pCallback, bool failure)
    {
        if (!failure && pCallback.m_bSuccess != 0)
        {
            Debug.Log("SCORE SUCCESS");

        }
        else
            Debug.Log("YOU SUCK RISHABH, YOU CANT EVEN GET THIS STUPID SCOREBOARD WORKING :( ");
    }

#endif
}
