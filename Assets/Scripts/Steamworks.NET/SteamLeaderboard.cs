#if !(UNITY_STANDALONE_WIN || UNITY_STANDALONE_LINUX || UNITY_STANDALONE_OSX || STEAMWORKS_WIN || STEAMWORKS_LIN_OSX)
#define DISABLESTEAMWORKS
#endif


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SocialPlatforms.Impl;
using TMPro;
using Unity.VisualScripting;


#if !DISABLESTEAMWORKS
using Steamworks;
#endif

public class SteamLeaderboard : MonoBehaviour
{
#if !DISABLESTEAMWORKS
    [SerializeField] string s_leaderboardName;

    private SteamLeaderboard_t s_currentLeaderboard;
    private bool s_initialized = false;
    private CallResult<LeaderboardFindResult_t> m_findResult = new CallResult<LeaderboardFindResult_t>();
    private CallResult<LeaderboardScoreUploaded_t> m_uploadResult = new CallResult<LeaderboardScoreUploaded_t>();
    private CallResult<LeaderboardScoresDownloaded_t> m_downloadResult = new CallResult<LeaderboardScoresDownloaded_t>();

    private int _previousScore; // THIS IS THE PREVIOUS SCORE OF THE USER 
    private bool _isScoreExist;

    public struct LeaderboardData
    {
        public string username;
        public int rank;
        public int score;
    }
    List<LeaderboardData> LeaderboardDataset;

    [Header("LeaderBoard UI Elements")]
    [SerializeField] List<TextMeshProUGUI> _leaderboardRanks = new List<TextMeshProUGUI>();
    [SerializeField] List<TextMeshProUGUI> _leaderboardName = new List<TextMeshProUGUI>();
    [SerializeField] List<TextMeshProUGUI> _leaderboardScore = new List<TextMeshProUGUI>();

    

    private void Start()
    {
        
        _isScoreExist = false;

        if (SteamManager.Initialized)
        {
            string name = SteamFriends.GetPersonaName();
            Debug.Log(name);
            GetLeaderboard();
        }
    }
    public void GetLeaderboard()
    {
        
        if(SteamManager.Initialized)
        {
            SteamAPICall_t hSteamAPICall = SteamUserStats.FindLeaderboard(s_leaderboardName);
            m_findResult.Set(hSteamAPICall, GettingLeaderboard);
        }

    }

    public void GettingLeaderboard(LeaderboardFindResult_t pCallback, bool failure)
    {
        //Debug.Log($"Steam Leaderboard Find: Did it fail? {failure}, Found: {pCallback.m_bLeaderboardFound}, leaderboardID: {pCallback.m_hSteamLeaderboard.m_SteamLeaderboard} ");
        s_currentLeaderboard = pCallback.m_hSteamLeaderboard;
        s_initialized = true;

        //Debug.Log("LeaderboardFound! Time to Get Score ");

        //THIS LINE BELOW WAS THE WORKING CODE 
        //SteamAPICall_t hSteamAPICall = SteamUserStats.DownloadLeaderboardEntries(s_currentLeaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal, 1, 10);
        
        //THIS LINE IS FOR THE DYNAMIC LEADERBOARD
        SteamAPICall_t hSteamAPICall = SteamUserStats.DownloadLeaderboardEntries(s_currentLeaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobalAroundUser, -3, 3);
        m_downloadResult.Set(hSteamAPICall,GetScore);
    }


    public void GetScore(LeaderboardScoresDownloaded_t pCallback, bool failure)
    {
        string displaystr; 
        if (!failure)
        {
            //Debug.Log("No. of Entries : "+pCallback.m_cEntryCount);

            for (int i = 0; i < pCallback.m_cEntryCount; i++)
            {
                LeaderboardEntry_t entry;
                SteamUserStats.GetDownloadedLeaderboardEntry(pCallback.m_hSteamLeaderboardEntries, i, out entry, null, 0);
                
               

                // THIS WILL CHECK THE SCORE ? 
                if (entry.m_steamIDUser == SteamUser.GetSteamID())
                {
                    _previousScore = entry.m_nScore;
                    Debug.Log("PREVIOUS SCORE : "+_previousScore);
                    _isScoreExist = true; // Previous Score Does Exist So Compare 
                }
                else
                {
                    _isScoreExist=false; // Previous Score Does not Exist So Just Push the Score
                }


                //Debug.Log($"User {SteamFriends.GetFriendPersonaName(entry.m_steamIDUser)} - {entry.m_nScore} - rank: {entry.m_nGlobalRank}");

                if (_leaderboardRanks[i] != null)
                    _leaderboardRanks[i].text = entry.m_nGlobalRank + ". ";

                if (_leaderboardName[i] != null)
                    _leaderboardName[i].text = SteamFriends.GetFriendPersonaName(entry.m_steamIDUser).ToString();

                //this code will need to have the conversion of the score
                if (_leaderboardScore[i] != null)
                {
                    float displayscore =  entry.m_nScore;
                    displayscore = displayscore / 100;
                    displaystr = displayscore.ToString() + " s";
                    _leaderboardScore[i].text = displaystr;
                }
                    

                // HERE YOU CAN UPDATE THE UI LMAO // IM USING LMAO ALOT NOWADAYS 
            }

        }
        else
            Debug.Log("Failed to Retreive Leaderbaord Scores ");
    }

    // THIS FUNCTION WILL BE CALLED AS AN EVENT 
    public void UploadScore()
    {
        int score = GameManager.GetScore();
        
        // Need to add logic to Compare Shit 

        if(_previousScore==0)  // IF Score does not Exist 
        {
            // EXECUTE CODE 
            if (s_initialized)
            {
                Debug.Log("NO PREVIOUS SCORE EXIST");
                Debug.Log("PREVIOUS SCORE: "+ _previousScore);

                SteamAPICall_t handle = SteamUserStats.UploadLeaderboardScore(s_currentLeaderboard,                     // The leaderboard handle
                ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodForceUpdate, // How to handle score uploads (e.g., keep best, force update)
                score,                                 // The score to upload
                null,                                  // Optional: an array of details (e.g., extra data associated with the score)
                0                                      // Optional: the count of details if provided
            );
                m_uploadResult.Set(handle, ScoreUploaded);
            }
        }
        else
        {
            Debug.Log("Previous Score : " + _previousScore);
            Debug.Log("Previous CurrentScore : " + score);
            // Compare and then Execute Code 
            if (score < _previousScore)
            {
                if (s_initialized)
                {
                    SteamAPICall_t handle = SteamUserStats.UploadLeaderboardScore(s_currentLeaderboard,                     // The leaderboard handle
                    ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodForceUpdate, // How to handle score uploads (e.g., keep best, force update)
                    score,                                 // The score to upload
                    null,                                  // Optional: an array of details (e.g., extra data associated with the score)
                    0                                      // Optional: the count of details if provided
                );
                    m_uploadResult.Set(handle, ScoreUploaded);
                }
            }
        }
        // There has to be a smarter way of doing this but no Rishabh, you are a dumbassssss


        


        

    }

    public void ScoreUploaded(LeaderboardScoreUploaded_t pCallback, bool failure)
    {
        if (!failure && pCallback.m_bSuccess != 0)
        {
            //Debug.Log("SCORE SUCCESS");

        }
        else;
            //Debug.Log("YOU SUCK RISHABH, YOU CANT EVEN GET THIS STUPID SCOREBOARD WORKING :( ");
    }




#endif

}
